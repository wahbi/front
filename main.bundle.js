webpackJsonp(["main"],{

/***/ "../../../../../$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/account.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var _1 = __webpack_require__("../../../../../src/main/webapp/app/account/index.ts");
var KataSgAccountModule = /** @class */ (function () {
    function KataSgAccountModule() {
    }
    KataSgAccountModule = __decorate([
        core_1.NgModule({
            imports: [
                shared_1.KataSgSharedModule,
                router_1.RouterModule.forChild(_1.accountState)
            ],
            declarations: [
                _1.ActivateComponent,
                _1.RegisterComponent,
                _1.PasswordComponent,
                _1.PasswordStrengthBarComponent,
                _1.PasswordResetInitComponent,
                _1.PasswordResetFinishComponent,
                _1.SettingsComponent
            ],
            providers: [
                _1.Register,
                _1.ActivateService,
                _1.PasswordService,
                _1.PasswordResetInitService,
                _1.PasswordResetFinishService
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        })
    ], KataSgAccountModule);
    return KataSgAccountModule;
}());
exports.KataSgAccountModule = KataSgAccountModule;
//# sourceMappingURL=account.module.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/account.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var _1 = __webpack_require__("../../../../../src/main/webapp/app/account/index.ts");
var ACCOUNT_ROUTES = [
    _1.activateRoute,
    _1.passwordRoute,
    _1.passwordResetFinishRoute,
    _1.passwordResetInitRoute,
    _1.registerRoute,
    _1.settingsRoute
];
exports.accountState = [{
        path: '',
        children: ACCOUNT_ROUTES
    }];
//# sourceMappingURL=account.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/activate/activate.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <div class=\"row justify-content-center\">\n        <div class=\"col-md-8\">\n            <h1>Activation</h1>\n\n            <div class=\"alert alert-success\" *ngIf=\"success\">\n                <span><strong>Your user account has been activated.</strong> Please </span>\n                <a class=\"alert-link\" (click)=\"login()\">sign in</a>.\n            </div>\n\n            <div class=\"alert alert-danger\" *ngIf=\"error\">\n                <strong>Your user could not be activated.</strong> Please use the registration form to sign up.\n            </div>\n\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/activate/activate.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var activate_service_1 = __webpack_require__("../../../../../src/main/webapp/app/account/activate/activate.service.ts");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var ActivateComponent = /** @class */ (function () {
    function ActivateComponent(activateService, loginModalService, route) {
        this.activateService = activateService;
        this.loginModalService = loginModalService;
        this.route = route;
    }
    ActivateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.activateService.get(params['key']).subscribe(function () {
                _this.error = null;
                _this.success = 'OK';
            }, function () {
                _this.success = null;
                _this.error = 'ERROR';
            });
        });
    };
    ActivateComponent.prototype.login = function () {
        this.modalRef = this.loginModalService.open();
    };
    ActivateComponent = __decorate([
        core_1.Component({
            selector: 'jhi-activate',
            template: __webpack_require__("../../../../../src/main/webapp/app/account/activate/activate.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof activate_service_1.ActivateService !== "undefined" && activate_service_1.ActivateService) === "function" && _a || Object, typeof (_b = typeof shared_1.LoginModalService !== "undefined" && shared_1.LoginModalService) === "function" && _b || Object, typeof (_c = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _c || Object])
    ], ActivateComponent);
    return ActivateComponent;
    var _a, _b, _c;
}());
exports.ActivateComponent = ActivateComponent;
//# sourceMappingURL=activate.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/activate/activate.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var activate_component_1 = __webpack_require__("../../../../../src/main/webapp/app/account/activate/activate.component.ts");
exports.activateRoute = {
    path: 'activate',
    component: activate_component_1.ActivateComponent,
    data: {
        authorities: [],
        pageTitle: 'Activation'
    },
    canActivate: [shared_1.UserRouteAccessService]
};
//# sourceMappingURL=activate.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/activate/activate.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var app_constants_1 = __webpack_require__("../../../../../src/main/webapp/app/app.constants.ts");
var ActivateService = /** @class */ (function () {
    function ActivateService(http) {
        this.http = http;
    }
    ActivateService.prototype.get = function (key) {
        var params = new http_1.URLSearchParams();
        params.set('key', key);
        return this.http.get(app_constants_1.SERVER_API_URL + 'api/activate', {
            search: params
        }).map(function (res) { return res; });
    };
    ActivateService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], ActivateService);
    return ActivateService;
    var _a;
}());
exports.ActivateService = ActivateService;
//# sourceMappingURL=activate.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/index.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__("../../../../../src/main/webapp/app/account/activate/activate.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/activate/activate.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/activate/activate.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/password/password.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/password/password-strength-bar.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/password/password.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/password/password.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/password-reset/finish/password-reset-finish.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/password-reset/finish/password-reset-finish.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/password-reset/finish/password-reset-finish.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/password-reset/init/password-reset-init.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/password-reset/init/password-reset-init.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/password-reset/init/password-reset-init.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/register/register.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/register/register.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/register/register.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/settings/settings.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/settings/settings.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/account/account.route.ts"));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password-reset/finish/password-reset-finish.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <div class=\"row justify-content-center\">\n        <div class=\"col-md-4\">\n            <h1>Reset password</h1>\n\n            <div class=\"alert alert-danger\" *ngIf=\"keyMissing\">\n                <strong>The password reset key is missing.</strong>\n            </div>\n\n            <div class=\"alert alert-warning\" *ngIf=\"!success && !keyMissing\">\n                <p>Choose a new password</p>\n            </div>\n\n            <div class=\"alert alert-danger\" *ngIf=\"error\">\n                <p>Your password couldn't be reset. Remember a password request is only valid for 24 hours.</p>\n            </div>\n\n            <p class=\"alert alert-success\" *ngIf=\"success\">\n                <span><strong>Your password has been reset.</strong> Please </span>\n                <a class=\"alert-link\" (click)=\"login()\">sign in</a>.\n            </p>\n\n            <div class=\"alert alert-danger\" *ngIf=\"doNotMatch\">\n                The password and its confirmation do not match!\n            </div>\n\n            <div *ngIf=\"!keyMissing\">\n                <form *ngIf=\"!success\" name=\"form\" role=\"form\" (ngSubmit)=\"finishReset()\" #passwordForm=\"ngForm\">\n                    <div class=\"form-group\">\n                        <label class=\"form-control-label\" for=\"password\">New password</label>\n                        <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" #passwordInput=\"ngModel\"\n                               placeholder=\"New password\"\n                               [(ngModel)]=\"resetAccount.password\" minlength=4 maxlength=50 required>\n                        <div *ngIf=\"passwordInput.dirty && passwordInput.invalid\">\n                            <small class=\"form-text text-danger\"\n                               *ngIf=\"passwordInput.errors.required\">\n                                Your password is required.\n                            </small>\n                            <small class=\"form-text text-danger\"\n                               *ngIf=\"passwordInput.errors.minlength\">\n                                Your password is required to be at least 4 characters.\n                            </small>\n                            <small class=\"form-text text-danger\"\n                               *ngIf=\"passwordInput.errors.maxlength\">\n                                Your password cannot be longer than 50 characters.\n                            </small>\n                        </div>\n                        <jhi-password-strength-bar [passwordToCheck]=\"resetAccount.password\"></jhi-password-strength-bar>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <label class=\"form-control-label\" for=\"confirmPassword\">New password confirmation</label>\n                        <input type=\"password\" class=\"form-control\" id=\"confirmPassword\" name=\"confirmPassword\" #confirmPasswordInput=\"ngModel\"\n                               placeholder=\"Confirm the new password\"\n                               [(ngModel)]=\"confirmPassword\" minlength=4 maxlength=50 required>\n                        <div *ngIf=\"confirmPasswordInput.dirty && confirmPasswordInput.invalid\">\n                            <small class=\"form-text text-danger\"\n                               *ngIf=\"confirmPasswordInput.errors.required\">\n                                Your password confirmation is required.\n                            </small>\n                            <small class=\"form-text text-danger\"\n                               *ngIf=\"confirmPasswordInput.errors.minlength\">\n                                Your password confirmation is required to be at least 4 characters.\n                            </small>\n                            <small class=\"form-text text-danger\"\n                               *ngIf=\"confirmPasswordInput.errors.maxlength\">\n                                Your password confirmation cannot be longer than 50 characters.\n                            </small>\n                        </div>\n                    </div>\n                    <button type=\"submit\" [disabled]=\"passwordForm.form.invalid\" class=\"btn btn-primary\">Reset Password</button>\n                </form>\n            </div>\n\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password-reset/finish/password-reset-finish.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var password_reset_finish_service_1 = __webpack_require__("../../../../../src/main/webapp/app/account/password-reset/finish/password-reset-finish.service.ts");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var PasswordResetFinishComponent = /** @class */ (function () {
    function PasswordResetFinishComponent(passwordResetFinishService, loginModalService, route, elementRef, renderer) {
        this.passwordResetFinishService = passwordResetFinishService;
        this.loginModalService = loginModalService;
        this.route = route;
        this.elementRef = elementRef;
        this.renderer = renderer;
    }
    PasswordResetFinishComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.key = params['key'];
        });
        this.resetAccount = {};
        this.keyMissing = !this.key;
    };
    PasswordResetFinishComponent.prototype.ngAfterViewInit = function () {
        if (this.elementRef.nativeElement.querySelector('#password') != null) {
            this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#password'), 'focus', []);
        }
    };
    PasswordResetFinishComponent.prototype.finishReset = function () {
        var _this = this;
        this.doNotMatch = null;
        this.error = null;
        if (this.resetAccount.password !== this.confirmPassword) {
            this.doNotMatch = 'ERROR';
        }
        else {
            this.passwordResetFinishService.save({ key: this.key, newPassword: this.resetAccount.password }).subscribe(function () {
                _this.success = 'OK';
            }, function () {
                _this.success = null;
                _this.error = 'ERROR';
            });
        }
    };
    PasswordResetFinishComponent.prototype.login = function () {
        this.modalRef = this.loginModalService.open();
    };
    PasswordResetFinishComponent = __decorate([
        core_1.Component({
            selector: 'jhi-password-reset-finish',
            template: __webpack_require__("../../../../../src/main/webapp/app/account/password-reset/finish/password-reset-finish.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof password_reset_finish_service_1.PasswordResetFinishService !== "undefined" && password_reset_finish_service_1.PasswordResetFinishService) === "function" && _a || Object, typeof (_b = typeof shared_1.LoginModalService !== "undefined" && shared_1.LoginModalService) === "function" && _b || Object, typeof (_c = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _c || Object, typeof (_d = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _d || Object, typeof (_e = typeof core_1.Renderer !== "undefined" && core_1.Renderer) === "function" && _e || Object])
    ], PasswordResetFinishComponent);
    return PasswordResetFinishComponent;
    var _a, _b, _c, _d, _e;
}());
exports.PasswordResetFinishComponent = PasswordResetFinishComponent;
//# sourceMappingURL=password-reset-finish.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password-reset/finish/password-reset-finish.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var password_reset_finish_component_1 = __webpack_require__("../../../../../src/main/webapp/app/account/password-reset/finish/password-reset-finish.component.ts");
exports.passwordResetFinishRoute = {
    path: 'reset/finish',
    component: password_reset_finish_component_1.PasswordResetFinishComponent,
    data: {
        authorities: [],
        pageTitle: 'Password'
    },
    canActivate: [shared_1.UserRouteAccessService]
};
//# sourceMappingURL=password-reset-finish.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password-reset/finish/password-reset-finish.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var app_constants_1 = __webpack_require__("../../../../../src/main/webapp/app/app.constants.ts");
var PasswordResetFinishService = /** @class */ (function () {
    function PasswordResetFinishService(http) {
        this.http = http;
    }
    PasswordResetFinishService.prototype.save = function (keyAndPassword) {
        return this.http.post(app_constants_1.SERVER_API_URL + 'api/account/reset-password/finish', keyAndPassword);
    };
    PasswordResetFinishService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], PasswordResetFinishService);
    return PasswordResetFinishService;
    var _a;
}());
exports.PasswordResetFinishService = PasswordResetFinishService;
//# sourceMappingURL=password-reset-finish.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password-reset/init/password-reset-init.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <div class=\"row justify-content-center\">\n        <div class=\"col-md-8\">\n            <h1>Reset your password</h1>\n\n            <div class=\"alert alert-danger\" *ngIf=\"errorEmailNotExists\">\n                <strong>Email address isn't registered!</strong> Please check and try again.\n            </div>\n\n            <div class=\"alert alert-warning\" *ngIf=\"!success\">\n                <p>Enter the email address you used to register.</p>\n            </div>\n\n            <div class=\"alert alert-success\" *ngIf=\"success === 'OK'\">\n                <p>Check your emails for details on how to reset your password.</p>\n            </div>\n\n            <form *ngIf=\"!success\" name=\"form\" role=\"form\" (ngSubmit)=\"requestReset()\" #resetRequestForm=\"ngForm\">\n                <div class=\"form-group\">\n                    <label class=\"form-control-label\" for=\"email\">Email</label>\n                    <input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" placeholder=\"Your email\"\n                           [(ngModel)]=\"resetAccount.email\" minlength=5 maxlength=100 #emailInput=\"ngModel\" email required>\n                    <div *ngIf=\"emailInput.dirty && emailInput.invalid\">\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"emailInput.errors.required\">\n                            Your email is required.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"emailInput.errors.email\">\n                            Your email is invalid.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"emailInput.errors.minlength\">\n                            Your email is required to be at least 5 characters.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"emailInput.errors.maxlength\">\n                            Your email cannot be longer than 100 characters.\n                        </small>\n                    </div>\n                </div>\n                <button type=\"submit\" [disabled]=\"resetRequestForm.form.invalid\" class=\"btn btn-primary\">Reset</button>\n            </form>\n\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password-reset/init/password-reset-init.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var password_reset_init_service_1 = __webpack_require__("../../../../../src/main/webapp/app/account/password-reset/init/password-reset-init.service.ts");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var PasswordResetInitComponent = /** @class */ (function () {
    function PasswordResetInitComponent(passwordResetInitService, elementRef, renderer) {
        this.passwordResetInitService = passwordResetInitService;
        this.elementRef = elementRef;
        this.renderer = renderer;
    }
    PasswordResetInitComponent.prototype.ngOnInit = function () {
        this.resetAccount = {};
    };
    PasswordResetInitComponent.prototype.ngAfterViewInit = function () {
        this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#email'), 'focus', []);
    };
    PasswordResetInitComponent.prototype.requestReset = function () {
        var _this = this;
        this.error = null;
        this.errorEmailNotExists = null;
        this.passwordResetInitService.save(this.resetAccount.email).subscribe(function () {
            _this.success = 'OK';
        }, function (response) {
            _this.success = null;
            if (response.status === 400 && response.json().type === shared_1.EMAIL_NOT_FOUND_TYPE) {
                _this.errorEmailNotExists = 'ERROR';
            }
            else {
                _this.error = 'ERROR';
            }
        });
    };
    PasswordResetInitComponent = __decorate([
        core_1.Component({
            selector: 'jhi-password-reset-init',
            template: __webpack_require__("../../../../../src/main/webapp/app/account/password-reset/init/password-reset-init.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof password_reset_init_service_1.PasswordResetInitService !== "undefined" && password_reset_init_service_1.PasswordResetInitService) === "function" && _a || Object, typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object, typeof (_c = typeof core_1.Renderer !== "undefined" && core_1.Renderer) === "function" && _c || Object])
    ], PasswordResetInitComponent);
    return PasswordResetInitComponent;
    var _a, _b, _c;
}());
exports.PasswordResetInitComponent = PasswordResetInitComponent;
//# sourceMappingURL=password-reset-init.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password-reset/init/password-reset-init.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var password_reset_init_component_1 = __webpack_require__("../../../../../src/main/webapp/app/account/password-reset/init/password-reset-init.component.ts");
exports.passwordResetInitRoute = {
    path: 'reset/request',
    component: password_reset_init_component_1.PasswordResetInitComponent,
    data: {
        authorities: [],
        pageTitle: 'Password'
    },
    canActivate: [shared_1.UserRouteAccessService]
};
//# sourceMappingURL=password-reset-init.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password-reset/init/password-reset-init.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var app_constants_1 = __webpack_require__("../../../../../src/main/webapp/app/app.constants.ts");
var PasswordResetInitService = /** @class */ (function () {
    function PasswordResetInitService(http) {
        this.http = http;
    }
    PasswordResetInitService.prototype.save = function (mail) {
        return this.http.post(app_constants_1.SERVER_API_URL + 'api/account/reset-password/init', mail);
    };
    PasswordResetInitService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], PasswordResetInitService);
    return PasswordResetInitService;
    var _a;
}());
exports.PasswordResetInitService = PasswordResetInitService;
//# sourceMappingURL=password-reset-init.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password/password-strength-bar.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var PasswordStrengthBarComponent = /** @class */ (function () {
    function PasswordStrengthBarComponent(renderer, elementRef) {
        this.renderer = renderer;
        this.elementRef = elementRef;
        this.colors = ['#F00', '#F90', '#FF0', '#9F0', '#0F0'];
    }
    PasswordStrengthBarComponent.prototype.measureStrength = function (p) {
        var force = 0;
        var regex = /[$-/:-?{-~!"^_`\[\]]/g; // "
        var lowerLetters = /[a-z]+/.test(p);
        var upperLetters = /[A-Z]+/.test(p);
        var numbers = /[0-9]+/.test(p);
        var symbols = regex.test(p);
        var flags = [lowerLetters, upperLetters, numbers, symbols];
        var passedMatches = flags.filter(function (isMatchedFlag) {
            return isMatchedFlag === true;
        }).length;
        force += 2 * p.length + ((p.length >= 10) ? 1 : 0);
        force += passedMatches * 10;
        // penality (short password)
        force = (p.length <= 6) ? Math.min(force, 10) : force;
        // penality (poor variety of characters)
        force = (passedMatches === 1) ? Math.min(force, 10) : force;
        force = (passedMatches === 2) ? Math.min(force, 20) : force;
        force = (passedMatches === 3) ? Math.min(force, 40) : force;
        return force;
    };
    ;
    PasswordStrengthBarComponent.prototype.getColor = function (s) {
        var idx = 0;
        if (s <= 10) {
            idx = 0;
        }
        else if (s <= 20) {
            idx = 1;
        }
        else if (s <= 30) {
            idx = 2;
        }
        else if (s <= 40) {
            idx = 3;
        }
        else {
            idx = 4;
        }
        return { idx: idx + 1, col: this.colors[idx] };
    };
    ;
    Object.defineProperty(PasswordStrengthBarComponent.prototype, "passwordToCheck", {
        set: function (password) {
            if (password) {
                var c = this.getColor(this.measureStrength(password));
                var element = this.elementRef.nativeElement;
                if (element.className) {
                    this.renderer.setElementClass(element, element.className, false);
                }
                var lis = element.getElementsByTagName('li');
                for (var i = 0; i < lis.length; i++) {
                    if (i < c.idx) {
                        this.renderer.setElementStyle(lis[i], 'backgroundColor', c.col);
                    }
                    else {
                        this.renderer.setElementStyle(lis[i], 'backgroundColor', '#DDD');
                    }
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Input(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], PasswordStrengthBarComponent.prototype, "passwordToCheck", null);
    PasswordStrengthBarComponent = __decorate([
        core_1.Component({
            selector: 'jhi-password-strength-bar',
            template: "\n        <div id=\"strength\">\n            <small>Password strength:</small>\n            <ul id=\"strengthBar\">\n                <li class=\"point\"></li>\n                <li class=\"point\"></li>\n                <li class=\"point\"></li>\n                <li class=\"point\"></li>\n                <li class=\"point\"></li>\n            </ul>\n        </div>",
            styles: [__webpack_require__("../../../../../src/main/webapp/app/account/password/password-strength-bar.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof core_1.Renderer !== "undefined" && core_1.Renderer) === "function" && _a || Object, typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object])
    ], PasswordStrengthBarComponent);
    return PasswordStrengthBarComponent;
    var _a, _b;
}());
exports.PasswordStrengthBarComponent = PasswordStrengthBarComponent;
//# sourceMappingURL=password-strength-bar.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password/password-strength-bar.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* ==========================================================================\nstart Password strength bar style\n========================================================================== */\nul#strengthBar {\n    display:inline;\n    list-style:none;\n    margin:0;\n    margin-left:15px;\n    padding:0;\n    vertical-align:2px;\n}\n\n.point:last {\n    margin:0 !important;\n}\n.point {\n    background:#DDD;\n    border-radius:2px;\n    display:inline-block;\n    height:5px;\n    margin-right:1px;\n    width:20px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password/password.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <div class=\"row justify-content-center\">\n        <div class=\"col-md-8\">\n            <h2 *ngIf=\"account\">Password for [<b>{{account.login}}</b>]</h2>\n\n            <div class=\"alert alert-success\" *ngIf=\"success\">\n                <strong>Password changed!</strong>\n            </div>\n            <div class=\"alert alert-danger\" *ngIf=\"error\" >\n                <strong>An error has occurred!</strong> The password could not be changed.\n            </div>\n\n            <div class=\"alert alert-danger\" *ngIf=\"doNotMatch\">\n                The password and its confirmation do not match!\n            </div>\n\n            <form name=\"form\" role=\"form\" (ngSubmit)=\"changePassword()\" #passwordForm=\"ngForm\">\n\n                <div class=\"form-group\">\n                    <label class=\"form-control-label\" for=\"password\">New password</label>\n                    <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" #passwordInput=\"ngModel\"\n                    placeholder=\"New password\"\n                           [(ngModel)]=\"password\" minlength=4 maxlength=50 required>\n                    <div *ngIf=\"passwordInput.dirty && passwordInput.invalid\">\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"passwordInput.errors.required\">\n                            Your password is required.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"passwordInput.errors.minlength\">\n                            Your password is required to be at least 4 characters.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"passwordInput.errors.maxlength\">\n                            Your password cannot be longer than 50 characters.\n                        </small>\n                    </div>\n                    <jhi-password-strength-bar [passwordToCheck]=\"password\"></jhi-password-strength-bar>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"form-control-label\" for=\"confirmPassword\">New password confirmation</label>\n                    <input type=\"password\" class=\"form-control\" id=\"confirmPassword\" name=\"confirmPassword\" #confirmPasswordInput=\"ngModel\"\n                    placeholder=\"Confirm the new password\"\n                           [(ngModel)]=\"confirmPassword\" minlength=4 maxlength=50 required>\n                    <div *ngIf=\"confirmPasswordInput.dirty && confirmPasswordInput.invalid\">\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"confirmPasswordInput.errors.required\">\n                            Your confirmation password is required.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"confirmPasswordInput.errors.minlength\">\n                            Your confirmation password is required to be at least 4 characters.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"confirmPasswordInput.errors.maxlength\">\n                            Your confirmation password cannot be longer than 50 characters.\n                        </small>\n                    </div>\n                </div>\n\n                <button type=\"submit\" [disabled]=\"passwordForm.form.invalid\" class=\"btn btn-primary\">Save</button>\n            </form>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password/password.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var password_service_1 = __webpack_require__("../../../../../src/main/webapp/app/account/password/password.service.ts");
var PasswordComponent = /** @class */ (function () {
    function PasswordComponent(passwordService, principal) {
        this.passwordService = passwordService;
        this.principal = principal;
    }
    PasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.principal.identity().then(function (account) {
            _this.account = account;
        });
    };
    PasswordComponent.prototype.changePassword = function () {
        var _this = this;
        if (this.password !== this.confirmPassword) {
            this.error = null;
            this.success = null;
            this.doNotMatch = 'ERROR';
        }
        else {
            this.doNotMatch = null;
            this.passwordService.save(this.password).subscribe(function () {
                _this.error = null;
                _this.success = 'OK';
            }, function () {
                _this.success = null;
                _this.error = 'ERROR';
            });
        }
    };
    PasswordComponent = __decorate([
        core_1.Component({
            selector: 'jhi-password',
            template: __webpack_require__("../../../../../src/main/webapp/app/account/password/password.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof password_service_1.PasswordService !== "undefined" && password_service_1.PasswordService) === "function" && _a || Object, typeof (_b = typeof shared_1.Principal !== "undefined" && shared_1.Principal) === "function" && _b || Object])
    ], PasswordComponent);
    return PasswordComponent;
    var _a, _b;
}());
exports.PasswordComponent = PasswordComponent;
//# sourceMappingURL=password.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password/password.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var password_component_1 = __webpack_require__("../../../../../src/main/webapp/app/account/password/password.component.ts");
exports.passwordRoute = {
    path: 'password',
    component: password_component_1.PasswordComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Password'
    },
    canActivate: [shared_1.UserRouteAccessService]
};
//# sourceMappingURL=password.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/password/password.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var app_constants_1 = __webpack_require__("../../../../../src/main/webapp/app/app.constants.ts");
var PasswordService = /** @class */ (function () {
    function PasswordService(http) {
        this.http = http;
    }
    PasswordService.prototype.save = function (newPassword) {
        return this.http.post(app_constants_1.SERVER_API_URL + 'api/account/change-password', newPassword);
    };
    PasswordService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], PasswordService);
    return PasswordService;
    var _a;
}());
exports.PasswordService = PasswordService;
//# sourceMappingURL=password.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <div class=\"row justify-content-center\">\n        <div class=\"col-md-8\">\n            <h1>Registration</h1>\n\n            <div class=\"alert alert-success\" *ngIf=\"success\">\n                <strong>Registration saved!</strong> Please check your email for confirmation.\n            </div>\n\n            <div class=\"alert alert-danger\" *ngIf=\"error\">\n                <strong>Registration failed!</strong> Please try again later.\n            </div>\n\n            <div class=\"alert alert-danger\" *ngIf=\"errorUserExists\">\n                <strong>Login name already registered!</strong> Please choose another one.\n            </div>\n\n            <div class=\"alert alert-danger\" *ngIf=\"errorEmailExists\">\n                <strong>Email is already in use!</strong> Please choose another one.\n            </div>\n\n            <div class=\"alert alert-danger\" *ngIf=\"doNotMatch\">\n                The password and its confirmation do not match!\n            </div>\n        </div>\n    </div>\n    <div class=\"row justify-content-center\">\n        <div class=\"col-md-8\">\n            <form name=\"form\" role=\"form\" (ngSubmit)=\"register()\" #registerForm=\"ngForm\" *ngIf=\"!success\">\n                <div class=\"form-group\">\n                    <label class=\"form-control-label\" for=\"login\">Username</label>\n                    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"registerAccount.login\" id=\"login\" name=\"login\" #login=\"ngModel\" placeholder=\"Your username\"\n                            required minlength=\"1\" maxlength=\"50\" pattern=\"^[_'.@A-Za-z0-9-]*$\">\n                    <div *ngIf=\"login.dirty && login.invalid\">\n                        <small class=\"form-text text-danger\" *ngIf=\"login.errors.required\">\n                            Your username is required.\n                        </small>\n                        <small class=\"form-text text-danger\" *ngIf=\"login.errors.minlength\"\n                               >\n                            Your username is required to be at least 1 character.\n                        </small>\n                        <small class=\"form-text text-danger\" *ngIf=\"login.errors.maxlength\"\n                               >\n                            Your username cannot be longer than 50 characters.\n                        </small>\n                        <small class=\"form-text text-danger\" *ngIf=\"login.errors.pattern\"\n                              >\n                            Your username can only contain lower-case letters and digits.\n                        </small>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"form-control-label\" for=\"email\">Email</label>\n                    <input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" #email=\"ngModel\" placeholder=\"Your email\"\n                            [(ngModel)]=\"registerAccount.email\" minlength=5 maxlength=100 email required>\n                    <div *ngIf=\"email.dirty && email.invalid\">\n                        <small class=\"form-text text-danger\" *ngIf=\"email.errors.required\"\n                               >\n                            Your email is required.\n                        </small>\n                        <small class=\"form-text text-danger\" *ngIf=\"email.errors.invalid\"\n                              >\n                            Your email is invalid.\n                        </small>\n                        <small class=\"form-text text-danger\" *ngIf=\"email.errors.minlength\"\n                              >\n                            Your email is required to be at least 5 characters.\n                        </small>\n                        <small class=\"form-text text-danger\" *ngIf=\"email.errors.maxlength\"\n                              >\n                            Your email cannot be longer than 100 characters.\n                        </small>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"form-control-label\" for=\"password\">New password</label>\n                    <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" #password=\"ngModel\" placeholder=\"New password\"\n                            [(ngModel)]=\"registerAccount.password\" minlength=4 maxlength=50 required>\n                    <div *ngIf=\"password.dirty && password.invalid\">\n                        <small class=\"form-text text-danger\" *ngIf=\"password.errors.required\"\n                               >\n                            Your password is required.\n                        </small>\n                        <small class=\"form-text text-danger\" *ngIf=\"password.errors.minlength\"\n                               >\n                            Your password is required to be at least 4 characters.\n                        </small>\n                        <small class=\"form-text text-danger\" *ngIf=\"password.errors.maxlength\"\n                               >\n                            Your password cannot be longer than 50 characters.\n                        </small>\n                    </div>\n                    <jhi-password-strength-bar [passwordToCheck]=\"registerAccount.password\"></jhi-password-strength-bar>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"form-control-label\" for=\"confirmPassword\">New password confirmation</label>\n                    <input type=\"password\" class=\"form-control\" id=\"confirmPassword\" name=\"confirmPassword\" #confirmPasswordInput=\"ngModel\" placeholder=\"Confirm the new password\"\n                            [(ngModel)]=\"confirmPassword\" minlength=4 maxlength=50 required>\n                    <div *ngIf=\"confirmPasswordInput.dirty && confirmPasswordInput.invalid\">\n                        <small class=\"form-text text-danger\" *ngIf=\"confirmPasswordInput.errors.required\"\n                              >\n                            Your confirmation password is required.\n                        </small>\n                        <small class=\"form-text text-danger\" *ngIf=\"confirmPasswordInput.errors.minlength\"\n                             >\n                            Your confirmation password is required to be at least 4 characters.\n                        </small>\n                        <small class=\"form-text text-danger\" *ngIf=\"confirmPasswordInput.errors.maxlength\"\n                              >\n                            Your confirmation password cannot be longer than 50 characters.\n                        </small>\n                    </div>\n                </div>\n\n                <button type=\"submit\" [disabled]=\"registerForm.form.invalid\" class=\"btn btn-primary\">Register</button>\n            </form>\n            <p></p>\n            <div class=\"alert alert-warning\">\n                <span>If you want to </span>\n                <a class=\"alert-link\" (click)=\"openLogin()\">sign in</a><span>, you can try the default accounts:<br/>- Administrator (login=\"admin\" and password=\"admin\") <br/>- User (login=\"user\" and password=\"user\").</span>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/register/register.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var register_service_1 = __webpack_require__("../../../../../src/main/webapp/app/account/register/register.service.ts");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(loginModalService, registerService, elementRef, renderer) {
        this.loginModalService = loginModalService;
        this.registerService = registerService;
        this.elementRef = elementRef;
        this.renderer = renderer;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.success = false;
        this.registerAccount = {};
    };
    RegisterComponent.prototype.ngAfterViewInit = function () {
        this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#login'), 'focus', []);
    };
    RegisterComponent.prototype.register = function () {
        var _this = this;
        if (this.registerAccount.password !== this.confirmPassword) {
            this.doNotMatch = 'ERROR';
        }
        else {
            this.doNotMatch = null;
            this.error = null;
            this.errorUserExists = null;
            this.errorEmailExists = null;
            this.registerAccount.langKey = 'en';
            this.registerService.save(this.registerAccount).subscribe(function () {
                _this.success = true;
            }, function (response) { return _this.processError(response); });
        }
    };
    RegisterComponent.prototype.openLogin = function () {
        this.modalRef = this.loginModalService.open();
    };
    RegisterComponent.prototype.processError = function (response) {
        this.success = null;
        if (response.status === 400 && response.json().type === shared_1.LOGIN_ALREADY_USED_TYPE) {
            this.errorUserExists = 'ERROR';
        }
        else if (response.status === 400 && response.json().type === shared_1.EMAIL_ALREADY_USED_TYPE) {
            this.errorEmailExists = 'ERROR';
        }
        else {
            this.error = 'ERROR';
        }
    };
    RegisterComponent = __decorate([
        core_1.Component({
            selector: 'jhi-register',
            template: __webpack_require__("../../../../../src/main/webapp/app/account/register/register.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof shared_1.LoginModalService !== "undefined" && shared_1.LoginModalService) === "function" && _a || Object, typeof (_b = typeof register_service_1.Register !== "undefined" && register_service_1.Register) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object, typeof (_d = typeof core_1.Renderer !== "undefined" && core_1.Renderer) === "function" && _d || Object])
    ], RegisterComponent);
    return RegisterComponent;
    var _a, _b, _c, _d;
}());
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/register/register.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var register_component_1 = __webpack_require__("../../../../../src/main/webapp/app/account/register/register.component.ts");
exports.registerRoute = {
    path: 'register',
    component: register_component_1.RegisterComponent,
    data: {
        authorities: [],
        pageTitle: 'Registration'
    }
};
//# sourceMappingURL=register.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/register/register.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var app_constants_1 = __webpack_require__("../../../../../src/main/webapp/app/app.constants.ts");
var Register = /** @class */ (function () {
    function Register(http) {
        this.http = http;
    }
    Register.prototype.save = function (account) {
        return this.http.post(app_constants_1.SERVER_API_URL + 'api/register', account);
    };
    Register = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], Register);
    return Register;
    var _a;
}());
exports.Register = Register;
//# sourceMappingURL=register.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/settings/settings.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <div class=\"row justify-content-center\">\n        <div class=\"col-md-8\">\n            <h2 *ngIf=\"settingsAccount\">User settings for [<b>{{settingsAccount.login}}</b>]</h2>\n\n            <div class=\"alert alert-success\" *ngIf=\"success\">\n                <strong>Settings saved!</strong>\n            </div>\n\n            <jhi-alert-error></jhi-alert-error>\n\n            <form name=\"form\" role=\"form\" (ngSubmit)=\"save()\" #settingsForm=\"ngForm\" *ngIf=\"settingsAccount\" novalidate>\n\n                <div class=\"form-group\">\n                    <label class=\"form-control-label\" for=\"firstName\">First Name</label>\n                    <input type=\"text\" class=\"form-control\" id=\"firstName\" name=\"firstName\" placeholder=\"Your first name\"\n                           [(ngModel)]=\"settingsAccount.firstName\" minlength=1 maxlength=50 #firstNameInput=\"ngModel\" required>\n                    <div *ngIf=\"firstNameInput.dirty && firstNameInput.invalid\">\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"firstNameInput.errors.required\">\n                            Your first name is required.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"firstNameInput.errors.minlength\">\n                            Your first name is required to be at least 1 character.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"firstNameInput.errors.maxlength\">\n                            Your first name cannot be longer than 50 characters.\n                        </small>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"form-control-label\" for=\"lastName\">Last Name</label>\n                    <input type=\"text\" class=\"form-control\" id=\"lastName\" name=\"lastName\" placeholder=\"Your last name\"\n                           [(ngModel)]=\"settingsAccount.lastName\" minlength=1 maxlength=50 #lastNameInput=\"ngModel\" required>\n                    <div *ngIf=\"lastNameInput.dirty && lastNameInput.invalid\">\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"lastNameInput.errors.required\">\n                            Your last name is required.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"lastNameInput.errors.minlength\">\n                            Your last name is required to be at least 1 character.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"lastNameInput.errors.maxlength\">\n                            Your last name cannot be longer than 50 characters.\n                        </small>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"form-control-label\" for=\"email\">Email</label>\n                    <input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" placeholder=\"Your email\"\n                           [(ngModel)]=\"settingsAccount.email\" minlength=\"5\" maxlength=\"100\" #emailInput=\"ngModel\" email required>\n                    <div *ngIf=\"emailInput.dirty && emailInput.invalid\">\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"emailInput.errors.required\">\n                            Your email is required.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"emailInput.errors.email\">\n                            Your email is invalid.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"emailInput.errors.minlength\">\n                            Your email is required to be at least 5 characters.\n                        </small>\n                        <small class=\"form-text text-danger\"\n                           *ngIf=\"emailInput.errors.maxlength\">\n                            Your email cannot be longer than 100 characters.\n                        </small>\n                    </div>\n                </div>\n                <button type=\"submit\" [disabled]=\"settingsForm.form.invalid\" class=\"btn btn-primary\">Save</button>\n            </form>\n        </div>\n    </div>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/settings/settings.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var SettingsComponent = /** @class */ (function () {
    function SettingsComponent(account, principal) {
        this.account = account;
        this.principal = principal;
    }
    SettingsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.principal.identity().then(function (account) {
            _this.settingsAccount = _this.copyAccount(account);
        });
    };
    SettingsComponent.prototype.save = function () {
        var _this = this;
        this.account.save(this.settingsAccount).subscribe(function () {
            _this.error = null;
            _this.success = 'OK';
            _this.principal.identity(true).then(function (account) {
                _this.settingsAccount = _this.copyAccount(account);
            });
        }, function () {
            _this.success = null;
            _this.error = 'ERROR';
        });
    };
    SettingsComponent.prototype.copyAccount = function (account) {
        return {
            activated: account.activated,
            email: account.email,
            firstName: account.firstName,
            langKey: account.langKey,
            lastName: account.lastName,
            login: account.login,
            imageUrl: account.imageUrl
        };
    };
    SettingsComponent = __decorate([
        core_1.Component({
            selector: 'jhi-settings',
            template: __webpack_require__("../../../../../src/main/webapp/app/account/settings/settings.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof shared_1.AccountService !== "undefined" && shared_1.AccountService) === "function" && _a || Object, typeof (_b = typeof shared_1.Principal !== "undefined" && shared_1.Principal) === "function" && _b || Object])
    ], SettingsComponent);
    return SettingsComponent;
    var _a, _b;
}());
exports.SettingsComponent = SettingsComponent;
//# sourceMappingURL=settings.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/account/settings/settings.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var settings_component_1 = __webpack_require__("../../../../../src/main/webapp/app/account/settings/settings.component.ts");
exports.settingsRoute = {
    path: 'settings',
    component: settings_component_1.SettingsComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Settings'
    },
    canActivate: [shared_1.UserRouteAccessService]
};
//# sourceMappingURL=settings.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/admin.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
/* jhipster-needle-add-admin-module-import - JHipster will add admin modules imports here */
var _1 = __webpack_require__("../../../../../src/main/webapp/app/admin/index.ts");
var KataSgAdminModule = /** @class */ (function () {
    function KataSgAdminModule() {
    }
    KataSgAdminModule = __decorate([
        core_1.NgModule({
            imports: [
                shared_1.KataSgSharedModule,
                router_1.RouterModule.forChild(_1.adminState),
            ],
            declarations: [
                _1.AuditsComponent,
                _1.UserMgmtComponent,
                _1.UserDialogComponent,
                _1.UserDeleteDialogComponent,
                _1.UserMgmtDetailComponent,
                _1.UserMgmtDialogComponent,
                _1.UserMgmtDeleteDialogComponent,
                _1.LogsComponent,
                _1.JhiConfigurationComponent,
                _1.JhiHealthCheckComponent,
                _1.JhiHealthModalComponent,
                _1.JhiDocsComponent,
                _1.JhiMetricsMonitoringComponent,
                _1.JhiMetricsMonitoringModalComponent
            ],
            entryComponents: [
                _1.UserMgmtDialogComponent,
                _1.UserMgmtDeleteDialogComponent,
                _1.JhiHealthModalComponent,
                _1.JhiMetricsMonitoringModalComponent,
            ],
            providers: [
                _1.AuditsService,
                _1.JhiConfigurationService,
                _1.JhiHealthService,
                _1.JhiMetricsService,
                _1.LogsService,
                _1.UserResolvePagingParams,
                _1.UserResolve,
                _1.UserModalService
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        })
    ], KataSgAdminModule);
    return KataSgAdminModule;
}());
exports.KataSgAdminModule = KataSgAdminModule;
//# sourceMappingURL=admin.module.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/admin.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var _1 = __webpack_require__("../../../../../src/main/webapp/app/admin/index.ts");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var ADMIN_ROUTES = [
    _1.auditsRoute,
    _1.configurationRoute,
    _1.docsRoute,
    _1.healthRoute,
    _1.logsRoute
].concat(_1.userMgmtRoute, [
    _1.metricsRoute
]);
exports.adminState = [{
        path: '',
        data: {
            authorities: ['ROLE_ADMIN']
        },
        canActivate: [shared_1.UserRouteAccessService],
        children: ADMIN_ROUTES
    }].concat(_1.userDialogRoute);
//# sourceMappingURL=admin.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/audits/audit-data.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var AuditData = /** @class */ (function () {
    function AuditData(remoteAddress, sessionId) {
        this.remoteAddress = remoteAddress;
        this.sessionId = sessionId;
    }
    return AuditData;
}());
exports.AuditData = AuditData;
//# sourceMappingURL=audit-data.model.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/audits/audit.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Audit = /** @class */ (function () {
    function Audit(data, principal, timestamp, type) {
        this.data = data;
        this.principal = principal;
        this.timestamp = timestamp;
        this.type = type;
    }
    return Audit;
}());
exports.Audit = Audit;
//# sourceMappingURL=audit.model.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/audits/audits.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"audits\">\n    <h2>Audits</h2>\n\n    <div class=\"row\">\n        <div class=\"col-md-5\">\n            <h4>Filter by date</h4>\n            <p class=\"d-flex\">\n                <span class=\"input-group-addon\">from</span>\n                <input type=\"date\" class=\"form-control\" name=\"start\" [(ngModel)]=\"fromDate\" (ngModelChange)=\"onChangeDate($event)\" required/>\n                <span class=\"input-group-addon\">to</span>\n                <input type=\"date\" class=\"form-control\" name=\"end\" [(ngModel)]=\"toDate\" (ngModelChange)=\"onChangeDate($event)\" required/>\n            </p>\n        </div>\n    </div>\n\n    <div class=\"table-responsive\">\n        <table class=\"table table-sm table-striped table-bordered\">\n            <thead>\n            <tr>\n                <th (click)=\"orderProp = 'timestamp'; reverse=!reverse\"><span>Date</span></th>\n                <th (click)=\"orderProp = 'principal'; reverse=!reverse\"><span>User</span></th>\n                <th (click)=\"orderProp = 'type'; reverse=!reverse\"><span>State</span></th>\n                <th (click)=\"orderProp = 'data.message'; reverse=!reverse\"><span>Extra data</span></th>\n            </tr>\n            </thead>\n            <tr *ngFor=\"let audit of getAudits()\">\n                <td><span>{{audit.timestamp| date:'medium'}}</span></td>\n                <td><small>{{audit.principal}}</small></td>\n                <td>{{audit.type}}</td>\n                <td>\n                    <span *ngIf=\"audit.data\" ng-show=\"audit.data.message\">{{audit.data.message}}</span>\n                    <span *ngIf=\"audit.data\" ng-show=\"audit.data.remoteAddress\"><span>Remote Address</span> {{audit.data.remoteAddress}}</span>\n                </td>\n            </tr>\n        </table>\n    </div>\n    <div *ngIf=\"audits\">\n        <div class=\"row justify-content-center\">\n            <jhi-item-count [page]=\"page\" [total]=\"totalItems\" [itemsPerPage]=\"itemsPerPage\"></jhi-item-count>\n        </div>\n        <div class=\"row justify-content-center\">\n            <ngb-pagination [collectionSize]=\"totalItems\" [(page)]=\"page\" (pageChange)=\"loadPage(page)\"></ngb-pagination>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/audits/audits.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var common_1 = __webpack_require__("../../../common/@angular/common.es5.js");
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var audits_service_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/audits/audits.service.ts");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var AuditsComponent = /** @class */ (function () {
    function AuditsComponent(auditsService, parseLinks) {
        this.auditsService = auditsService;
        this.parseLinks = parseLinks;
        this.itemsPerPage = shared_1.ITEMS_PER_PAGE;
        this.page = 1;
        this.reverse = false;
        this.orderProp = 'timestamp';
        this.datePipe = new common_1.DatePipe('en');
    }
    AuditsComponent.prototype.getAudits = function () {
        return this.sortAudits(this.audits);
    };
    AuditsComponent.prototype.loadPage = function (page) {
        this.page = page;
        this.onChangeDate();
    };
    AuditsComponent.prototype.ngOnInit = function () {
        this.today();
        this.previousMonth();
        this.onChangeDate();
    };
    AuditsComponent.prototype.onChangeDate = function () {
        var _this = this;
        this.auditsService.query({ page: this.page - 1, size: this.itemsPerPage,
            fromDate: this.fromDate, toDate: this.toDate }).subscribe(function (res) {
            _this.audits = res.json();
            _this.links = _this.parseLinks.parse(res.headers.get('link'));
            _this.totalItems = +res.headers.get('X-Total-Count');
        });
    };
    AuditsComponent.prototype.previousMonth = function () {
        var dateFormat = 'yyyy-MM-dd';
        var fromDate = new Date();
        if (fromDate.getMonth() === 0) {
            fromDate = new Date(fromDate.getFullYear() - 1, 11, fromDate.getDate());
        }
        else {
            fromDate = new Date(fromDate.getFullYear(), fromDate.getMonth() - 1, fromDate.getDate());
        }
        this.fromDate = this.datePipe.transform(fromDate, dateFormat);
    };
    AuditsComponent.prototype.today = function () {
        var dateFormat = 'yyyy-MM-dd';
        // Today + 1 day - needed if the current day must be included
        var today = new Date();
        today.setDate(today.getDate() + 1);
        var date = new Date(today.getFullYear(), today.getMonth(), today.getDate());
        this.toDate = this.datePipe.transform(date, dateFormat);
    };
    AuditsComponent.prototype.sortAudits = function (audits) {
        var _this = this;
        audits = audits.slice(0).sort(function (a, b) {
            if (a[_this.orderProp] < b[_this.orderProp]) {
                return -1;
            }
            else if (true) {
                return 1;
            }
            else {
                return 0;
            }
        });
        return this.reverse ? audits.reverse() : audits;
    };
    AuditsComponent = __decorate([
        core_1.Component({
            selector: 'jhi-audit',
            template: __webpack_require__("../../../../../src/main/webapp/app/admin/audits/audits.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof audits_service_1.AuditsService !== "undefined" && audits_service_1.AuditsService) === "function" && _a || Object, typeof (_b = typeof ng_jhipster_1.JhiParseLinks !== "undefined" && ng_jhipster_1.JhiParseLinks) === "function" && _b || Object])
    ], AuditsComponent);
    return AuditsComponent;
    var _a, _b;
}());
exports.AuditsComponent = AuditsComponent;
//# sourceMappingURL=audits.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/audits/audits.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var audits_component_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/audits/audits.component.ts");
exports.auditsRoute = {
    path: 'audits',
    component: audits_component_1.AuditsComponent,
    data: {
        pageTitle: 'Audits'
    }
};
//# sourceMappingURL=audits.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/audits/audits.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var AuditsService = /** @class */ (function () {
    function AuditsService(http) {
        this.http = http;
    }
    AuditsService.prototype.query = function (req) {
        var params = new http_1.URLSearchParams();
        params.set('fromDate', req.fromDate);
        params.set('toDate', req.toDate);
        params.set('page', req.page);
        params.set('size', req.size);
        params.set('sort', req.sort);
        var options = {
            search: params
        };
        return this.http.get('management/audits', options);
    };
    AuditsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], AuditsService);
    return AuditsService;
    var _a;
}());
exports.AuditsService = AuditsService;
//# sourceMappingURL=audits.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/configuration/configuration.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"allConfiguration && configuration\">\n    <h2>Configuration</h2>\n\n    <span>Filter (by prefix)</span> <input type=\"text\" [(ngModel)]=\"filter\" class=\"form-control\">\n    <label>Spring configuration</label>\n    <table class=\"table table-striped table-bordered table-responsive d-table\">\n        <thead>\n        <tr>\n            <th class=\"w-40\" (click)=\"orderProp = 'prefix'; reverse=!reverse\"><span>Prefix</span></th>\n            <th class=\"w-60\" (click)=\"orderProp = 'properties'; reverse=!reverse\"><span>Properties</span></th>\n        </tr>\n        </thead>\n        <tbody>\n        <tr *ngFor=\"let entry of (configuration | pureFilter:filter:'prefix' | orderBy:orderProp:reverse)\">\n            <td><span>{{entry.prefix}}</span></td>\n            <td>\n                <div class=\"row\" *ngFor=\"let key of keys(entry.properties)\">\n                    <div class=\"col-md-4\">{{key}}</div>\n                    <div class=\"col-md-8\">\n                        <span class=\"float-right badge badge-secondary break\">{{entry.properties[key] | json}}</span>\n                    </div>\n                </div>\n            </td>\n        </tr>\n        </tbody>\n    </table>\n    <div *ngFor=\"let key of keys(allConfiguration)\">\n        <label><span>{{key}}</span></label>\n        <table class=\"table table-sm table-striped table-bordered table-responsive d-table\">\n            <thead>\n            <tr>\n                <th class=\"w-40\">Property</th>\n                <th class=\"w-60\">Value</th>\n            </tr>\n            </thead>\n            <tbody>\n            <tr *ngFor=\"let item of allConfiguration[key]\">\n                <td class=\"break\">{{item.key}}</td>\n                <td class=\"break\">\n                    <span class=\"float-right badge badge-secondary break\">{{item.val}}</span>\n                </td>\n            </tr>\n            </tbody>\n        </table>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/configuration/configuration.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var configuration_service_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/configuration/configuration.service.ts");
var JhiConfigurationComponent = /** @class */ (function () {
    function JhiConfigurationComponent(configurationService) {
        this.configurationService = configurationService;
        this.allConfiguration = null;
        this.configuration = null;
        this.configKeys = [];
        this.filter = '';
        this.orderProp = 'prefix';
        this.reverse = false;
    }
    JhiConfigurationComponent.prototype.keys = function (dict) {
        return (dict === undefined) ? [] : Object.keys(dict);
    };
    JhiConfigurationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.configurationService.get().subscribe(function (configuration) {
            _this.configuration = configuration;
            for (var _i = 0, configuration_1 = configuration; _i < configuration_1.length; _i++) {
                var config = configuration_1[_i];
                if (config.properties !== undefined) {
                    _this.configKeys.push(Object.keys(config.properties));
                }
            }
        });
        this.configurationService.getEnv().subscribe(function (configuration) {
            _this.allConfiguration = configuration;
        });
    };
    JhiConfigurationComponent = __decorate([
        core_1.Component({
            selector: 'jhi-configuration',
            template: __webpack_require__("../../../../../src/main/webapp/app/admin/configuration/configuration.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof configuration_service_1.JhiConfigurationService !== "undefined" && configuration_service_1.JhiConfigurationService) === "function" && _a || Object])
    ], JhiConfigurationComponent);
    return JhiConfigurationComponent;
    var _a;
}());
exports.JhiConfigurationComponent = JhiConfigurationComponent;
//# sourceMappingURL=configuration.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/configuration/configuration.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var configuration_component_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/configuration/configuration.component.ts");
exports.configurationRoute = {
    path: 'jhi-configuration',
    component: configuration_component_1.JhiConfigurationComponent,
    data: {
        pageTitle: 'Configuration'
    }
};
//# sourceMappingURL=configuration.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/configuration/configuration.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var JhiConfigurationService = /** @class */ (function () {
    function JhiConfigurationService(http) {
        this.http = http;
    }
    JhiConfigurationService.prototype.get = function () {
        return this.http.get('management/configprops').map(function (res) {
            var properties = [];
            var propertiesObject = res.json();
            for (var key in propertiesObject) {
                if (propertiesObject.hasOwnProperty(key)) {
                    properties.push(propertiesObject[key]);
                }
            }
            return properties.sort(function (propertyA, propertyB) {
                return (propertyA.prefix === propertyB.prefix) ? 0 :
                    (propertyA.prefix < propertyB.prefix) ? -1 : 1;
            });
        });
    };
    JhiConfigurationService.prototype.getEnv = function () {
        return this.http.get('management/env').map(function (res) {
            var properties = {};
            var propertiesObject = res.json();
            for (var key in propertiesObject) {
                if (propertiesObject.hasOwnProperty(key)) {
                    var valsObject = propertiesObject[key];
                    var vals = [];
                    for (var valKey in valsObject) {
                        if (valsObject.hasOwnProperty(valKey)) {
                            vals.push({ key: valKey, val: valsObject[valKey] });
                        }
                    }
                    properties[key] = vals;
                }
            }
            return properties;
        });
    };
    JhiConfigurationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], JhiConfigurationService);
    return JhiConfigurationService;
    var _a;
}());
exports.JhiConfigurationService = JhiConfigurationService;
//# sourceMappingURL=configuration.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/docs/docs.component.html":
/***/ (function(module, exports) {

module.exports = "<iframe src=\"swagger-ui/index.html\" width=\"100%\" height=\"900\" seamless\n    target=\"_top\" title=\"Swagger UI\" class=\"border-0\"></iframe>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/docs/docs.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var JhiDocsComponent = /** @class */ (function () {
    function JhiDocsComponent() {
    }
    JhiDocsComponent = __decorate([
        core_1.Component({
            selector: 'jhi-docs',
            template: __webpack_require__("../../../../../src/main/webapp/app/admin/docs/docs.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], JhiDocsComponent);
    return JhiDocsComponent;
}());
exports.JhiDocsComponent = JhiDocsComponent;
//# sourceMappingURL=docs.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/docs/docs.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var docs_component_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/docs/docs.component.ts");
exports.docsRoute = {
    path: 'docs',
    component: docs_component_1.JhiDocsComponent,
    data: {
        pageTitle: 'API'
    }
};
//# sourceMappingURL=docs.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/health/health-modal.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\n    <h4 class=\"modal-title\" id=\"showHealthLabel\"><span class=\"text-capitalize\">{{ baseName(currentHealth.name) }}</span>\n        {{subSystemName(currentHealth.name)}}\n    </h4>\n    <button aria-label=\"Close\" data-dismiss=\"modal\" class=\"close\" type=\"button\" (click)=\"activeModal.dismiss('closed')\"><span aria-hidden=\"true\">&times;</span>\n    </button>\n</div>\n<div class=\"modal-body pad\">\n    <div *ngIf=\"currentHealth.details\">\n        <h5>Properties</h5>\n        <div class=\"table-responsive\">\n            <table class=\"table table-striped\">\n                <thead>\n                    <tr>\n                        <th class=\"text-left\">Name</th>\n                        <th class=\"text-left\">Value</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let entry of currentHealth.details | keys\">\n                        <td class=\"text-left\">{{entry.key}}</td>\n                        <td class=\"text-left\">{{readableValue(entry.value)}}</td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n    </div>\n    <div *ngIf=\"currentHealth.error\">\n        <h4>Error</h4>\n            <pre>{{currentHealth.error}}</pre>\n    </div>\n</div>\n<div class=\"modal-footer\">\n    <button data-dismiss=\"modal\" class=\"btn btn-secondary float-left\" type=\"button\" (click)=\"activeModal.dismiss('closed')\">Done</button>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/health/health-modal.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var health_service_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/health/health.service.ts");
var JhiHealthModalComponent = /** @class */ (function () {
    function JhiHealthModalComponent(healthService, activeModal) {
        this.healthService = healthService;
        this.activeModal = activeModal;
    }
    JhiHealthModalComponent.prototype.baseName = function (name) {
        return this.healthService.getBaseName(name);
    };
    JhiHealthModalComponent.prototype.subSystemName = function (name) {
        return this.healthService.getSubSystemName(name);
    };
    JhiHealthModalComponent.prototype.readableValue = function (value) {
        if (this.currentHealth.name !== 'diskSpace') {
            return value.toString();
        }
        // Should display storage space in an human readable unit
        var val = value / 1073741824;
        if (val > 1) {
            return val.toFixed(2) + ' GB';
        }
        else {
            return (value / 1048576).toFixed(2) + ' MB';
        }
    };
    JhiHealthModalComponent = __decorate([
        core_1.Component({
            selector: 'jhi-health-modal',
            template: __webpack_require__("../../../../../src/main/webapp/app/admin/health/health-modal.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof health_service_1.JhiHealthService !== "undefined" && health_service_1.JhiHealthService) === "function" && _a || Object, typeof (_b = typeof ng_bootstrap_1.NgbActiveModal !== "undefined" && ng_bootstrap_1.NgbActiveModal) === "function" && _b || Object])
    ], JhiHealthModalComponent);
    return JhiHealthModalComponent;
    var _a, _b;
}());
exports.JhiHealthModalComponent = JhiHealthModalComponent;
//# sourceMappingURL=health-modal.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/health/health.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <h2>\n        <span>Health Checks</span>\n        <button class=\"btn btn-primary float-right\" (click)=\"refresh()\">\n            <span class=\"fa fa-refresh\"></span> <span>Refresh</span>\n        </button>\n    </h2>\n    <div class=\"table-responsive\">\n        <table id=\"healthCheck\" class=\"table table-striped\">\n            <thead>\n                <tr>\n                    <th>Service Name</th>\n                    <th class=\"text-center\">Status</th>\n                    <th class=\"text-center\">Details</th>\n                </tr>\n            </thead>\n            <tbody>\n                <tr *ngFor=\"let health of healthData\">\n                    <td><span class=\"text-capitalize\">{{ baseName(health.name) }}</span> {{subSystemName(health.name)}}</td>\n                    <td class=\"text-center\">\n                        <span class=\"badge\" [ngClass]=\"getBadgeClass(health.status)\">\n                            {{health.status}}\n                        </span>\n                    </td>\n                    <td class=\"text-center\">\n                        <a class=\"hand\" (click)=\"showHealth(health)\" *ngIf=\"health.details || health.error\">\n                            <i class=\"fa fa-eye\"></i>\n                        </a>\n                    </td>\n                </tr>\n            </tbody>\n        </table>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/health/health.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var health_service_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/health/health.service.ts");
var health_modal_component_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/health/health-modal.component.ts");
var JhiHealthCheckComponent = /** @class */ (function () {
    function JhiHealthCheckComponent(modalService, healthService) {
        this.modalService = modalService;
        this.healthService = healthService;
    }
    JhiHealthCheckComponent.prototype.ngOnInit = function () {
        this.refresh();
    };
    JhiHealthCheckComponent.prototype.baseName = function (name) {
        return this.healthService.getBaseName(name);
    };
    JhiHealthCheckComponent.prototype.getBadgeClass = function (statusState) {
        if (statusState === 'UP') {
            return 'badge-success';
        }
        else {
            return 'badge-danger';
        }
    };
    JhiHealthCheckComponent.prototype.refresh = function () {
        var _this = this;
        this.updatingHealth = true;
        this.healthService.checkHealth().subscribe(function (health) {
            _this.healthData = _this.healthService.transformHealthData(health);
            _this.updatingHealth = false;
        }, function (error) {
            if (error.status === 503) {
                _this.healthData = _this.healthService.transformHealthData(error.json());
                _this.updatingHealth = false;
            }
        });
    };
    JhiHealthCheckComponent.prototype.showHealth = function (health) {
        var modalRef = this.modalService.open(health_modal_component_1.JhiHealthModalComponent);
        modalRef.componentInstance.currentHealth = health;
        modalRef.result.then(function (result) {
            // Left blank intentionally, nothing to do here
        }, function (reason) {
            // Left blank intentionally, nothing to do here
        });
    };
    JhiHealthCheckComponent.prototype.subSystemName = function (name) {
        return this.healthService.getSubSystemName(name);
    };
    JhiHealthCheckComponent = __decorate([
        core_1.Component({
            selector: 'jhi-health',
            template: __webpack_require__("../../../../../src/main/webapp/app/admin/health/health.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof ng_bootstrap_1.NgbModal !== "undefined" && ng_bootstrap_1.NgbModal) === "function" && _a || Object, typeof (_b = typeof health_service_1.JhiHealthService !== "undefined" && health_service_1.JhiHealthService) === "function" && _b || Object])
    ], JhiHealthCheckComponent);
    return JhiHealthCheckComponent;
    var _a, _b;
}());
exports.JhiHealthCheckComponent = JhiHealthCheckComponent;
//# sourceMappingURL=health.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/health/health.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var health_component_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/health/health.component.ts");
exports.healthRoute = {
    path: 'jhi-health',
    component: health_component_1.JhiHealthCheckComponent,
    data: {
        pageTitle: 'Health Checks'
    }
};
//# sourceMappingURL=health.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/health/health.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var JhiHealthService = /** @class */ (function () {
    function JhiHealthService(http) {
        this.http = http;
        this.separator = '.';
    }
    JhiHealthService.prototype.checkHealth = function () {
        return this.http.get('management/health').map(function (res) { return res.json(); });
    };
    JhiHealthService.prototype.transformHealthData = function (data) {
        var response = [];
        this.flattenHealthData(response, null, data);
        return response;
    };
    JhiHealthService.prototype.getBaseName = function (name) {
        if (name) {
            var split = name.split('.');
            return split[0];
        }
    };
    JhiHealthService.prototype.getSubSystemName = function (name) {
        if (name) {
            var split = name.split('.');
            split.splice(0, 1);
            var remainder = split.join('.');
            return remainder ? ' - ' + remainder : '';
        }
    };
    /* private methods */
    JhiHealthService.prototype.addHealthObject = function (result, isLeaf, healthObject, name) {
        var healthData = {
            name: name
        };
        var details = {};
        var hasDetails = false;
        for (var key in healthObject) {
            if (healthObject.hasOwnProperty(key)) {
                var value = healthObject[key];
                if (key === 'status' || key === 'error') {
                    healthData[key] = value;
                }
                else {
                    if (!this.isHealthObject(value)) {
                        details[key] = value;
                        hasDetails = true;
                    }
                }
            }
        }
        // Add the details
        if (hasDetails) {
            healthData.details = details;
        }
        // Only add nodes if they provide additional information
        if (isLeaf || hasDetails || healthData.error) {
            result.push(healthData);
        }
        return healthData;
    };
    JhiHealthService.prototype.flattenHealthData = function (result, path, data) {
        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                var value = data[key];
                if (this.isHealthObject(value)) {
                    if (this.hasSubSystem(value)) {
                        this.addHealthObject(result, false, value, this.getModuleName(path, key));
                        this.flattenHealthData(result, this.getModuleName(path, key), value);
                    }
                    else {
                        this.addHealthObject(result, true, value, this.getModuleName(path, key));
                    }
                }
            }
        }
        return result;
    };
    JhiHealthService.prototype.getModuleName = function (path, name) {
        var result;
        if (path && name) {
            result = path + this.separator + name;
        }
        else if (path) {
            result = path;
        }
        else if (name) {
            result = name;
        }
        else {
            result = '';
        }
        return result;
    };
    JhiHealthService.prototype.hasSubSystem = function (healthObject) {
        var result = false;
        for (var key in healthObject) {
            if (healthObject.hasOwnProperty(key)) {
                var value = healthObject[key];
                if (value && value.status) {
                    result = true;
                }
            }
        }
        return result;
    };
    JhiHealthService.prototype.isHealthObject = function (healthObject) {
        var result = false;
        for (var key in healthObject) {
            if (healthObject.hasOwnProperty(key)) {
                if (key === 'status') {
                    result = true;
                }
            }
        }
        return result;
    };
    JhiHealthService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], JhiHealthService);
    return JhiHealthService;
    var _a;
}());
exports.JhiHealthService = JhiHealthService;
//# sourceMappingURL=health.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/index.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/audits/audits.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/audits/audits.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/audits/audits.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/audits/audit.model.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/audits/audit-data.model.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/configuration/configuration.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/configuration/configuration.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/configuration/configuration.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/docs/docs.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/docs/docs.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/health/health.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/health/health-modal.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/health/health.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/health/health.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/logs/logs.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/logs/logs.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/logs/logs.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/logs/log.model.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/metrics/metrics.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/metrics/metrics-modal.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/metrics/metrics.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/metrics/metrics.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management-dialog.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management-delete-dialog.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management-detail.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-modal.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/admin/admin.route.ts"));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/logs/log.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Log = /** @class */ (function () {
    function Log(name, level) {
        this.name = name;
        this.level = level;
    }
    return Log;
}());
exports.Log = Log;
//# sourceMappingURL=log.model.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/logs/logs.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"table-responsive\" *ngIf=\"loggers\">\n    <h2>Logs</h2>\n\n    <p>There are {{ loggers.length }} loggers.</p>\n\n    <span>Filter</span> <input type=\"text\" [(ngModel)]=\"filter\" class=\"form-control\">\n\n    <table class=\"table table-sm table-striped table-bordered\">\n        <thead>\n        <tr title=\"click to order\">\n            <th (click)=\"orderProp = 'name'; reverse=!reverse\"><span>Name</span></th>\n            <th (click)=\"orderProp = 'level'; reverse=!reverse\"><span>Level</span></th>\n        </tr>\n        </thead>\n\n        <tr *ngFor=\"let logger of (loggers | pureFilter:filter:'name' | orderBy:orderProp:reverse)\">\n            <td><small>{{logger.name | slice:0:140}}</small></td>\n            <td>\n                <button (click)=\"changeLevel(logger.name, 'TRACE')\" [ngClass]=\"(logger.level=='TRACE') ? 'btn-danger' : 'btn-light'\" class=\"btn btn-sm\">TRACE</button>\n                <button (click)=\"changeLevel(logger.name, 'DEBUG')\" [ngClass]=\"(logger.level=='DEBUG') ? 'btn-warning' : 'btn-light'\" class=\"btn btn-sm\">DEBUG</button>\n                <button (click)=\"changeLevel(logger.name, 'INFO')\" [ngClass]=\"(logger.level=='INFO') ? 'btn-info' : 'btn-light'\" class=\"btn btn-sm\">INFO</button>\n                <button (click)=\"changeLevel(logger.name, 'WARN')\" [ngClass]=\"(logger.level=='WARN') ? 'btn-success' : 'btn-light'\" class=\"btn btn-sm\">WARN</button>\n                <button (click)=\"changeLevel(logger.name, 'ERROR')\" [ngClass]=\"(logger.level=='ERROR') ? 'btn-primary' : 'btn-light'\" class=\"btn btn-sm\">ERROR</button>\n            </td>\n        </tr>\n    </table>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/logs/logs.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var log_model_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/logs/log.model.ts");
var logs_service_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/logs/logs.service.ts");
var LogsComponent = /** @class */ (function () {
    function LogsComponent(logsService) {
        this.logsService = logsService;
        this.filter = '';
        this.orderProp = 'name';
        this.reverse = false;
    }
    LogsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.logsService.findAll().subscribe(function (loggers) { return _this.loggers = loggers; });
    };
    LogsComponent.prototype.changeLevel = function (name, level) {
        var _this = this;
        var log = new log_model_1.Log(name, level);
        this.logsService.changeLevel(log).subscribe(function () {
            _this.logsService.findAll().subscribe(function (loggers) { return _this.loggers = loggers; });
        });
    };
    LogsComponent = __decorate([
        core_1.Component({
            selector: 'jhi-logs',
            template: __webpack_require__("../../../../../src/main/webapp/app/admin/logs/logs.component.html"),
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof logs_service_1.LogsService !== "undefined" && logs_service_1.LogsService) === "function" && _a || Object])
    ], LogsComponent);
    return LogsComponent;
    var _a;
}());
exports.LogsComponent = LogsComponent;
//# sourceMappingURL=logs.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/logs/logs.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var logs_component_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/logs/logs.component.ts");
exports.logsRoute = {
    path: 'logs',
    component: logs_component_1.LogsComponent,
    data: {
        pageTitle: 'Logs'
    }
};
//# sourceMappingURL=logs.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/logs/logs.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var LogsService = /** @class */ (function () {
    function LogsService(http) {
        this.http = http;
    }
    LogsService.prototype.changeLevel = function (log) {
        return this.http.put('management/logs', log);
    };
    LogsService.prototype.findAll = function () {
        return this.http.get('management/logs').map(function (res) { return res.json(); });
    };
    LogsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], LogsService);
    return LogsService;
    var _a;
}());
exports.LogsService = LogsService;
//# sourceMappingURL=logs.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/metrics/metrics-modal.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Modal used to display the threads dump -->\n<div class=\"modal-header\">\n    <h4 class=\"modal-title\">Threads dump</h4>\n    <button type=\"button\" class=\"close\" (click)=\"activeModal.dismiss('closed')\">&times;</button>\n</div>\n<div class=\"modal-body\">\n    <span class=\"badge badge-primary\" (click)=\"threadDumpFilter = {}\">All&nbsp;<span class=\"badge badge-pill badge-default\">{{threadDumpAll}}</span></span>&nbsp;\n    <span class=\"badge badge-success\" (click)=\"threadDumpFilter = {threadState: 'RUNNABLE'}\">Runnable&nbsp;<span class=\"badge badge-pill badge-default\">{{threadDumpRunnable}}</span></span>&nbsp;\n    <span class=\"badge badge-info\" (click)=\"threadDumpFilter = {threadState: 'WAITING'}\">Waiting&nbsp;<span class=\"badge badge-pill badge-default\">{{threadDumpWaiting}}</span></span>&nbsp;\n    <span class=\"badge badge-warning\" (click)=\"threadDumpFilter = {threadState: 'TIMED_WAITING'}\">Timed Waiting&nbsp;<span class=\"badge badge-pill badge-default\">{{threadDumpTimedWaiting}}</span></span>&nbsp;\n    <span class=\"badge badge-danger\" (click)=\"threadDumpFilter = {threadState: 'BLOCKED'}\">Blocked&nbsp;<span class=\"badge badge-pill badge-default\">{{threadDumpBlocked}}</span></span>&nbsp;\n    <div class=\"mt-2\">&nbsp;</div>\n    Filter\n    <input type=\"text\" [(ngModel)]=\"threadDumpFilter\" class=\"form-control\">\n    <div class=\"pad\" *ngFor=\"let entry of threadDump | pureFilter:threadDumpFilter:'lockName' | keys\">\n        <h6>\n            <span class=\"badge\" [ngClass]=\"getBadgeClass(entry.value.threadState)\">{{entry.value.threadState}}</span>&nbsp;{{entry.value.threadName}} (ID {{entry.value.threadId}})\n            <a (click)=\"entry.show = !entry.show\" href=\"javascript:void(0);\">\n               <span [hidden]=\"entry.show\">Show StackTrace</span>\n               <span [hidden]=\"!entry.show\">Hide StackTrace</span>\n            </a>\n        </h6>\n        <div class=\"card\" [hidden]=\"!entry.show\">\n            <div class=\"card-body\">\n                <div *ngFor=\"let st of entry.value.stackTrace | keys\" class=\"break\">\n                    <samp>{{st.value.className}}.{{st.value.methodName}}(<code>{{st.value.fileName}}:{{st.value.lineNumber}}</code>)</samp>\n                    <span class=\"mt-1\"></span>\n                </div>\n            </div>\n        </div>\n        <table class=\"table table-sm table-responsive\">\n            <thead>\n                <tr>\n                    <th>Blocked Time</th>\n                    <th>Blocked Count</th>\n                    <th>Waited Time</th>\n                    <th>Waited Count</th>\n                    <th>Lock Name</th>\n                </tr>\n            </thead>\n            <tbody>\n                <tr>\n                    <td>{{entry.value.blockedTime}}</td>\n                    <td>{{entry.value.blockedCount}}</td>\n                    <td>{{entry.value.waitedTime}}</td>\n                    <td>{{entry.value.waitedCount}}</td>\n                    <td class=\"thread-dump-modal-lock\" title=\"{{entry.value.lockName}}\"><code>{{entry.value.lockName}}</code></td>\n                </tr>\n            </tbody>\n        </table>\n\n    </div>\n</div>\n<div class=\"modal-footer\">\n    <button type=\"button\" class=\"btn btn-secondary float-left\" data-dismiss=\"modal\" (click)=\"activeModal.dismiss('closed')\">Done</button>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/metrics/metrics-modal.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var JhiMetricsMonitoringModalComponent = /** @class */ (function () {
    function JhiMetricsMonitoringModalComponent(activeModal) {
        this.activeModal = activeModal;
        this.threadDumpAll = 0;
        this.threadDumpBlocked = 0;
        this.threadDumpRunnable = 0;
        this.threadDumpTimedWaiting = 0;
        this.threadDumpWaiting = 0;
    }
    JhiMetricsMonitoringModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.threadDump.forEach(function (value) {
            if (value.threadState === 'RUNNABLE') {
                _this.threadDumpRunnable += 1;
            }
            else if (value.threadState === 'WAITING') {
                _this.threadDumpWaiting += 1;
            }
            else if (value.threadState === 'TIMED_WAITING') {
                _this.threadDumpTimedWaiting += 1;
            }
            else if (value.threadState === 'BLOCKED') {
                _this.threadDumpBlocked += 1;
            }
        });
        this.threadDumpAll = this.threadDumpRunnable + this.threadDumpWaiting +
            this.threadDumpTimedWaiting + this.threadDumpBlocked;
    };
    JhiMetricsMonitoringModalComponent.prototype.getBadgeClass = function (threadState) {
        if (threadState === 'RUNNABLE') {
            return 'badge-success';
        }
        else if (threadState === 'WAITING') {
            return 'badge-info';
        }
        else if (threadState === 'TIMED_WAITING') {
            return 'badge-warning';
        }
        else if (threadState === 'BLOCKED') {
            return 'badge-danger';
        }
    };
    JhiMetricsMonitoringModalComponent = __decorate([
        core_1.Component({
            selector: 'jhi-metrics-modal',
            template: __webpack_require__("../../../../../src/main/webapp/app/admin/metrics/metrics-modal.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof ng_bootstrap_1.NgbActiveModal !== "undefined" && ng_bootstrap_1.NgbActiveModal) === "function" && _a || Object])
    ], JhiMetricsMonitoringModalComponent);
    return JhiMetricsMonitoringModalComponent;
    var _a;
}());
exports.JhiMetricsMonitoringModalComponent = JhiMetricsMonitoringModalComponent;
//# sourceMappingURL=metrics-modal.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/metrics/metrics.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <h2>\n        <span>Application Metrics</span>\n        <button class=\"btn btn-primary float-right\" (click)=\"refresh()\">\n            <span class=\"fa fa-refresh\"></span> <span>Refresh</span>\n        </button>\n    </h2>\n\n    <h3>JVM Metrics</h3>\n    <div class=\"row\" *ngIf=\"!updatingMetrics\">\n        <div class=\"col-md-4\">\n            <b>Memory</b>\n            <p><span>Total Memory</span> ({{metrics.gauges['jvm.memory.total.used'].value / 1000000 | number:'1.0-0'}}M / {{metrics.gauges['jvm.memory.total.max'].value / 1000000 | number:'1.0-0'}}M)</p>\n            <ngb-progressbar type=\"success\" [max]=\"metrics.gauges['jvm.memory.total.max'].value\" [value]=\"metrics.gauges['jvm.memory.total.used'].value\" [striped]=\"true\" [animated]=\"false\">\n                <span>{{metrics.gauges['jvm.memory.total.used'].value * 100 / metrics.gauges['jvm.memory.total.max'].value  | number:'1.0-0'}}%</span>\n            </ngb-progressbar>\n            <p><span>Heap Memory</span> ({{metrics.gauges['jvm.memory.heap.used'].value / 1000000 | number:'1.0-0'}}M / {{metrics.gauges['jvm.memory.heap.max'].value / 1000000 | number:'1.0-0'}}M)</p>\n            <ngb-progressbar [max]=\"metrics.gauges['jvm.memory.heap.max'].value\" [value]=\"metrics.gauges['jvm.memory.heap.used'].value\" [striped]=\"true\" [animated]=\"false\" type=\"success\">\n                <span>{{metrics.gauges['jvm.memory.heap.used'].value * 100 / metrics.gauges['jvm.memory.heap.max'].value  | number:'1.0-0'}}%</span>\n            </ngb-progressbar>\n            <p><span>Non-Heap Memory</span> ({{metrics.gauges['jvm.memory.non-heap.used'].value / 1000000 | number:'1.0-0'}}M / {{metrics.gauges['jvm.memory.non-heap.committed'].value / 1000000 | number:'1.0-0'}}M)</p>\n            <ngb-progressbar [max]=\"metrics.gauges['jvm.memory.non-heap.committed'].value\" [value]=\"metrics.gauges['jvm.memory.non-heap.used'].value\" [striped]=\"true\" [animated]=\"false\" type=\"success\">\n                <span>{{metrics.gauges['jvm.memory.non-heap.used'].value * 100 / metrics.gauges['jvm.memory.non-heap.committed'].value  | number:'1.0-0'}}%</span>\n            </ngb-progressbar>\n        </div>\n        <div class=\"col-md-4\">\n            <b>Threads</b> (Total: {{metrics.gauges['jvm.threads.count'].value}}) <a class=\"hand\" (click)=\"refreshThreadDumpData()\" data-toggle=\"modal\" data-target=\"#threadDump\"><i class=\"fa fa-eye\"></i></a>\n            <p><span>Runnable</span> {{metrics.gauges['jvm.threads.runnable.count'].value}}</p>\n            <ngb-progressbar [value]=\"metrics.gauges['jvm.threads.runnable.count'].value\" [max]=\"metrics.gauges['jvm.threads.count'].value\" [striped]=\"true\" [animated]=\"false\" type=\"success\">\n                <span>{{metrics.gauges['jvm.threads.runnable.count'].value * 100 / metrics.gauges['jvm.threads.count'].value  | number:'1.0-0'}}%</span>\n            </ngb-progressbar>\n            <p><span>Timed Waiting</span> ({{metrics.gauges['jvm.threads.timed_waiting.count'].value}})</p>\n            <ngb-progressbar [value]=\"metrics.gauges['jvm.threads.timed_waiting.count'].value\" [max]=\"metrics.gauges['jvm.threads.count'].value\" [striped]=\"true\" [animated]=\"false\" type=\"warning\">\n                <span>{{metrics.gauges['jvm.threads.timed_waiting.count'].value * 100 / metrics.gauges['jvm.threads.count'].value  | number:'1.0-0'}}%</span>\n            </ngb-progressbar>\n            <p><span>Waiting</span> ({{metrics.gauges['jvm.threads.waiting.count'].value}})</p>\n            <ngb-progressbar [value]=\"metrics.gauges['jvm.threads.waiting.count'].value\" [max]=\"metrics.gauges['jvm.threads.count'].value\" [striped]=\"true\" [animated]=\"false\" type=\"warning\">\n                <span>{{metrics.gauges['jvm.threads.waiting.count'].value * 100 / metrics.gauges['jvm.threads.count'].value  | number:'1.0-0'}}%</span>\n            </ngb-progressbar>\n            <p><span>Blocked</span> ({{metrics.gauges['jvm.threads.blocked.count'].value}})</p>\n            <ngb-progressbar [value]=\"metrics.gauges['jvm.threads.blocked.count'].value\" [max]=\"metrics.gauges['jvm.threads.count'].value\" [striped]=\"true\" [animated]=\"false\" type=\"success\">\n                <span>{{metrics.gauges['jvm.threads.blocked.count'].value * 100 / metrics.gauges['jvm.threads.count'].value  | number:'1.0-0'}}%</span>\n            </ngb-progressbar>\n        </div>\n        <div class=\"col-md-4\">\n            <b>Garbage collections</b>\n            <div class=\"row\" *ngIf=\"metrics.gauges['jvm.garbage.PS-MarkSweep.count']\">\n                <div class=\"col-md-9\">Mark Sweep count</div>\n                <div class=\"col-md-3 text-right\">{{metrics.gauges['jvm.garbage.PS-MarkSweep.count'].value}}</div>\n            </div>\n            <div class=\"row\" *ngIf=\"metrics.gauges['jvm.garbage.PS-MarkSweep.time']\">\n                <div class=\"col-md-9\">Mark Sweep time</div>\n                <div class=\"col-md-3 text-right\">{{metrics.gauges['jvm.garbage.PS-MarkSweep.time'].value}}ms</div>\n            </div>\n            <div class=\"row\" *ngIf=\"metrics.gauges['jvm.garbage.PS-Scavenge.count']\">\n                <div class=\"col-md-9\">Scavenge count</div>\n                <div class=\"col-md-3 text-right\">{{metrics.gauges['jvm.garbage.PS-Scavenge.count'].value}}</div>\n            </div>\n            <div class=\"row\" *ngIf=\"metrics.gauges['jvm.garbage.PS-Scavenge.time']\">\n                <div class=\"col-md-9\">Scavenge time</div>\n                <div class=\"col-md-3 text-right\">{{metrics.gauges['jvm.garbage.PS-Scavenge.time'].value}}ms</div>\n            </div>\n        </div>\n    </div>\n    <div class=\"well well-lg\" *ngIf=\"updatingMetrics\">Updating...</div>\n\n    <h3>HTTP requests (events per second)</h3>\n    <p *ngIf=\"metrics.counters\">\n        <span>Active requests</span> <b>{{metrics.counters['com.codahale.metrics.servlet.InstrumentedFilter.activeRequests'].count | number:'1.0-0'}}</b> - <span>Total requests</span> <b>{{metrics.timers['com.codahale.metrics.servlet.InstrumentedFilter.requests'].count | number:'1.0-0'}}</b>\n    </p>\n    <div class=\"table-responsive\" *ngIf=\"!updatingMetrics\">\n        <table class=\"table table-striped\">\n            <thead>\n            <tr>\n                <th>Code</th>\n                <th>Count</th>\n                <th class=\"text-right\">Mean</th>\n                <th class=\"text-right\"><span>Average</span> (1 min)</th>\n                <th class=\"text-right\"><span>Average</span> (5 min)</th>\n                <th class=\"text-right\"><span>Average</span> (15 min)</th>\n            </tr>\n            </thead>\n            <tbody>\n            <tr>\n                <td>OK</td>\n                <td>\n                    <ngb-progressbar [max]=\"metrics.timers['com.codahale.metrics.servlet.InstrumentedFilter.requests'].count\" [value]=\"metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.ok'].count\" [striped]=\"true\" [animated]=\"false\" type=\"success\">\n                        <span>{{metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.ok'].count}}</span>\n                    </ngb-progressbar>\n                </td>\n                <td class=\"text-right\">\n                    {{filterNaN(metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.ok'].mean_rate) | number:'1.0-2'}}\n                </td>\n                <td class=\"text-right\">\n                    {{filterNaN(metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.ok'].m1_rate) | number:'1.0-2'}}\n                </td>\n                <td class=\"text-right\">\n                    {{filterNaN(metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.ok'].m5_rate) | number:'1.0-2'}}\n                </td>\n                <td class=\"text-right\">\n                    {{filterNaN(metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.ok'].m15_rate) | number:'1.0-2'}}\n                </td>\n            </tr>\n            <tr>\n                <td>Not Found</td>\n                <td>\n                    <ngb-progressbar [max]=\"metrics.timers['com.codahale.metrics.servlet.InstrumentedFilter.requests'].count\" [value]=\"metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.notFound'].count\" [striped]=\"true\" [animated]=\"false\" type=\"success\">\n                        <span>{{metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.notFound'].count}}</span>\n                    </ngb-progressbar>\n                </td>\n                <td class=\"text-right\">\n                    {{filterNaN(metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.notFound'].mean_rate) | number:'1.0-2'}}\n                </td>\n                <td class=\"text-right\">\n                    {{filterNaN(metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.notFound'].m1_rate) | number:'1.0-2'}}\n                </td>\n                <td class=\"text-right\">\n                    {{filterNaN(metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.notFound'].m5_rate) | number:'1.0-2'}}\n                </td>\n                <td class=\"text-right\">\n                    {{filterNaN(metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.notFound'].m15_rate) | number:'1.0-2'}}\n                </td>\n            </tr>\n            <tr>\n                <td>Server error</td>\n                <td>\n                    <ngb-progressbar [max]=\"metrics.timers['com.codahale.metrics.servlet.InstrumentedFilter.requests'].count\" [value]=\"metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.serverError'].count\" [striped]=\"true\" [animated]=\"false\" type=\"success\">\n                        <span>{{metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.serverError'].count}}</span>\n                    </ngb-progressbar>\n                </td>\n                <td class=\"text-right\">\n                    {{filterNaN(metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.serverError'].mean_rate) | number:'1.0-2'}}\n                </td>\n                <td class=\"text-right\">\n                    {{filterNaN(metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.serverError'].m1_rate) | number:'1.0-2'}}\n                </td>\n                <td class=\"text-right\">\n                    {{filterNaN(metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.serverError'].m5_rate) | number:'1.0-2'}}\n                </td>\n                <td class=\"text-right\">\n                    {{filterNaN(metrics.meters['com.codahale.metrics.servlet.InstrumentedFilter.responseCodes.serverError'].m15_rate) | number:'1.0-2'}}\n                </td>\n            </tr>\n            </tbody>\n        </table>\n    </div>\n\n    <h3>Services statistics (time in millisecond)</h3>\n    <div class=\"table-responsive\" *ngIf=\"!updatingMetrics\">\n        <table class=\"table table-striped\">\n            <thead>\n            <tr>\n                <th>Service name</th>\n                <th class=\"text-right\">Count</th>\n                <th class=\"text-right\">Mean</th>\n                <th class=\"text-right\">Min</th>\n                <th class=\"text-right\">p50</th>\n                <th class=\"text-right\">p75</th>\n                <th class=\"text-right\">p95</th>\n                <th class=\"text-right\">p99</th>\n                <th class=\"text-right\">Max</th>\n            </tr>\n            </thead>\n            <tbody>\n            <tr *ngFor=\"let entry of servicesStats | keys\">\n                <td>{{entry.key}}</td>\n                <td class=\"text-right\">{{entry.value.count}}</td>\n                <td class=\"text-right\">{{entry.value.mean * 1000 | number:'1.0-0'}}</td>\n                <td class=\"text-right\">{{entry.value.min * 1000 | number:'1.0-0'}}</td>\n                <td class=\"text-right\">{{entry.value.p50 * 1000 | number:'1.0-0'}}</td>\n                <td class=\"text-right\">{{entry.value.p75 * 1000 | number:'1.0-0'}}</td>\n                <td class=\"text-right\">{{entry.value.p95 * 1000 | number:'1.0-0'}}</td>\n                <td class=\"text-right\">{{entry.value.p99 * 1000 | number:'1.0-0'}}</td>\n                <td class=\"text-right\">{{entry.value.max * 1000 | number:'1.0-0'}}</td>\n            </tr>\n            </tbody>\n        </table>\n    </div>\n    \n    <h3 *ngIf=\"metrics.gauges && metrics.gauges['HikariPool-1.pool.TotalConnections'] && metrics.gauges['HikariPool-1.pool.TotalConnections'].value > 0\">DataSource statistics (time in millisecond)</h3>\n    <div class=\"table-responsive\" *ngIf=\"!updatingMetrics && metrics.gauges && metrics.gauges['HikariPool-1.pool.TotalConnections'] && metrics.gauges['HikariPool-1.pool.TotalConnections'].value > 0\">\n        <table class=\"table table-striped\">\n            <thead>\n                <tr>\n                    <th><span>Usage</span> ({{metrics.gauges['HikariPool-1.pool.ActiveConnections'].value}} / {{metrics.gauges['HikariPool-1.pool.TotalConnections'].value}})</th>\n                    <th class=\"text-right\">Count</th>\n                    <th class=\"text-right\">Mean</th>\n                    <th class=\"text-right\">Min</th>\n                    <th class=\"text-right\">p50</th>\n                    <th class=\"text-right\">p75</th>\n                    <th class=\"text-right\">p95</th>\n                    <th class=\"text-right\">p99</th>\n                    <th class=\"text-right\">Max</th>\n                </tr>\n            </thead>\n            <tbody>\n                <tr>\n                    <td>\n                        <div class=\"progress progress-striped\">\n                            <ngb-progressbar [max]=\"metrics.gauges['HikariPool-1.pool.TotalConnections'].value\" [value]=\"metrics.gauges['HikariPool-1.pool.ActiveConnections'].value\" [striped]=\"true\" [animated]=\"false\" type=\"success\">\n                                <span>{{metrics.gauges['HikariPool-1.pool.ActiveConnections'].value * 100 / metrics.gauges['HikariPool-1.pool.TotalConnections'].value  | number:'1.0-0'}}%</span>\n                            </ngb-progressbar>\n                        </div>\n                    </td>\n                    <td class=\"text-right\">{{metrics.histograms['HikariPool-1.pool.Usage'].count}}</td>\n                    <td class=\"text-right\">{{filterNaN(metrics.histograms['HikariPool-1.pool.Usage'].mean) | number:'1.0-2'}}</td>\n                    <td class=\"text-right\">{{filterNaN(metrics.histograms['HikariPool-1.pool.Usage'].min) | number:'1.0-2'}}</td>\n                    <td class=\"text-right\">{{filterNaN(metrics.histograms['HikariPool-1.pool.Usage'].p50) | number:'1.0-2'}}</td>\n                    <td class=\"text-right\">{{filterNaN(metrics.histograms['HikariPool-1.pool.Usage'].p75) | number:'1.0-2'}}</td>\n                    <td class=\"text-right\">{{filterNaN(metrics.histograms['HikariPool-1.pool.Usage'].p95) | number:'1.0-2'}}</td>\n                    <td class=\"text-right\">{{filterNaN(metrics.histograms['HikariPool-1.pool.Usage'].p99) | number:'1.0-2'}}</td>\n                    <td class=\"text-right\">{{filterNaN(metrics.histograms['HikariPool-1.pool.Usage'].max) | number:'1.0-2'}}</td>\n                </tr>\n            </tbody>\n        </table>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/metrics/metrics.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var metrics_modal_component_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/metrics/metrics-modal.component.ts");
var metrics_service_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/metrics/metrics.service.ts");
var JhiMetricsMonitoringComponent = /** @class */ (function () {
    function JhiMetricsMonitoringComponent(modalService, metricsService) {
        this.modalService = modalService;
        this.metricsService = metricsService;
        this.metrics = {};
        this.cachesStats = {};
        this.servicesStats = {};
        this.updatingMetrics = true;
        this.JCACHE_KEY = 'jcache.statistics';
    }
    JhiMetricsMonitoringComponent.prototype.ngOnInit = function () {
        this.refresh();
    };
    JhiMetricsMonitoringComponent.prototype.refresh = function () {
        var _this = this;
        this.updatingMetrics = true;
        this.metricsService.getMetrics().subscribe(function (metrics) {
            _this.metrics = metrics;
            _this.updatingMetrics = false;
            _this.servicesStats = {};
            _this.cachesStats = {};
            Object.keys(metrics.timers).forEach(function (key) {
                var value = metrics.timers[key];
                if (key.includes('web.rest') || key.includes('service')) {
                    _this.servicesStats[key] = value;
                }
            });
            Object.keys(metrics.gauges).forEach(function (key) {
                if (key.includes('jcache.statistics')) {
                    var value = metrics.gauges[key].value;
                    // remove gets or puts
                    var index = key.lastIndexOf('.');
                    var newKey = key.substr(0, index);
                    // Keep the name of the domain
                    _this.cachesStats[newKey] = {
                        'name': _this.JCACHE_KEY.length,
                        'value': value
                    };
                }
            });
        });
    };
    JhiMetricsMonitoringComponent.prototype.refreshThreadDumpData = function () {
        var _this = this;
        this.metricsService.threadDump().subscribe(function (data) {
            var modalRef = _this.modalService.open(metrics_modal_component_1.JhiMetricsMonitoringModalComponent, { size: 'lg' });
            modalRef.componentInstance.threadDump = data;
            modalRef.result.then(function (result) {
                // Left blank intentionally, nothing to do here
            }, function (reason) {
                // Left blank intentionally, nothing to do here
            });
        });
    };
    JhiMetricsMonitoringComponent.prototype.filterNaN = function (input) {
        if (isNaN(input)) {
            return 0;
        }
        return input;
    };
    JhiMetricsMonitoringComponent = __decorate([
        core_1.Component({
            selector: 'jhi-metrics',
            template: __webpack_require__("../../../../../src/main/webapp/app/admin/metrics/metrics.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof ng_bootstrap_1.NgbModal !== "undefined" && ng_bootstrap_1.NgbModal) === "function" && _a || Object, typeof (_b = typeof metrics_service_1.JhiMetricsService !== "undefined" && metrics_service_1.JhiMetricsService) === "function" && _b || Object])
    ], JhiMetricsMonitoringComponent);
    return JhiMetricsMonitoringComponent;
    var _a, _b;
}());
exports.JhiMetricsMonitoringComponent = JhiMetricsMonitoringComponent;
//# sourceMappingURL=metrics.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/metrics/metrics.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var metrics_component_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/metrics/metrics.component.ts");
exports.metricsRoute = {
    path: 'jhi-metrics',
    component: metrics_component_1.JhiMetricsMonitoringComponent,
    data: {
        pageTitle: 'Application Metrics'
    }
};
//# sourceMappingURL=metrics.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/metrics/metrics.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var JhiMetricsService = /** @class */ (function () {
    function JhiMetricsService(http) {
        this.http = http;
    }
    JhiMetricsService.prototype.getMetrics = function () {
        return this.http.get('management/metrics').map(function (res) { return res.json(); });
    };
    JhiMetricsService.prototype.threadDump = function () {
        return this.http.get('management/dump').map(function (res) { return res.json(); });
    };
    JhiMetricsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], JhiMetricsService);
    return JhiMetricsService;
    var _a;
}());
exports.JhiMetricsService = JhiMetricsService;
//# sourceMappingURL=metrics.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/user-management/user-management-delete-dialog.component.html":
/***/ (function(module, exports) {

module.exports = "<form name=\"deleteForm\" (ngSubmit)=\"confirmDelete(user.login)\">\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\">Confirm delete operation</h4>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\"\n            (click)=\"clear()\">&times;</button>\n    </div>\n    <div class=\"modal-body\">\n        <jhi-alert-error></jhi-alert-error>\n        <p>Are you sure you want to delete this User?</p>\n    </div>\n    <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\" (click)=\"clear()\">\n            <span class=\"fa fa-ban\"></span>&nbsp;<span>Cancel</span>\n        </button>\n        <button type=\"submit\" class=\"btn btn-danger\">\n            <span class=\"fa fa-remove\"></span>&nbsp;<span>Delete</span>\n        </button>\n    </div>\n</form>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/user-management/user-management-delete-dialog.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var user_modal_service_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-modal.service.ts");
var UserMgmtDeleteDialogComponent = /** @class */ (function () {
    function UserMgmtDeleteDialogComponent(userService, activeModal, eventManager) {
        this.userService = userService;
        this.activeModal = activeModal;
        this.eventManager = eventManager;
    }
    UserMgmtDeleteDialogComponent.prototype.clear = function () {
        this.activeModal.dismiss('cancel');
    };
    UserMgmtDeleteDialogComponent.prototype.confirmDelete = function (login) {
        var _this = this;
        this.userService.delete(login).subscribe(function (response) {
            _this.eventManager.broadcast({ name: 'userListModification',
                content: 'Deleted a user' });
            _this.activeModal.dismiss(true);
        });
    };
    UserMgmtDeleteDialogComponent = __decorate([
        core_1.Component({
            selector: 'jhi-user-mgmt-delete-dialog',
            template: __webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management-delete-dialog.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof shared_1.UserService !== "undefined" && shared_1.UserService) === "function" && _a || Object, typeof (_b = typeof ng_bootstrap_1.NgbActiveModal !== "undefined" && ng_bootstrap_1.NgbActiveModal) === "function" && _b || Object, typeof (_c = typeof ng_jhipster_1.JhiEventManager !== "undefined" && ng_jhipster_1.JhiEventManager) === "function" && _c || Object])
    ], UserMgmtDeleteDialogComponent);
    return UserMgmtDeleteDialogComponent;
    var _a, _b, _c;
}());
exports.UserMgmtDeleteDialogComponent = UserMgmtDeleteDialogComponent;
var UserDeleteDialogComponent = /** @class */ (function () {
    function UserDeleteDialogComponent(route, userModalService) {
        this.route = route;
        this.userModalService = userModalService;
    }
    UserDeleteDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeSub = this.route.params.subscribe(function (params) {
            _this.userModalService.open(UserMgmtDeleteDialogComponent, params['login']);
        });
    };
    UserDeleteDialogComponent.prototype.ngOnDestroy = function () {
        this.routeSub.unsubscribe();
    };
    UserDeleteDialogComponent = __decorate([
        core_1.Component({
            selector: 'jhi-user-delete-dialog',
            template: ''
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _a || Object, typeof (_b = typeof user_modal_service_1.UserModalService !== "undefined" && user_modal_service_1.UserModalService) === "function" && _b || Object])
    ], UserDeleteDialogComponent);
    return UserDeleteDialogComponent;
    var _a, _b;
}());
exports.UserDeleteDialogComponent = UserDeleteDialogComponent;
//# sourceMappingURL=user-management-delete-dialog.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/user-management/user-management-detail.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"user\">\n    <h2>\n        <span>User</span> [<b>{{user.login}}</b>]\n    </h2>\n    <dl class=\"row-md jh-entity-details\">\n        <dt><span>Login</span></dt>\n        <dd>\n            <span>{{user.login}}</span>\n            <span class=\"badge badge-danger\" *ngIf=\"!user.activated\"\n                 >Deactivated</span>\n            <span class=\"badge badge-success\" *ngIf=\"user.activated\"\n                 >Activated</span>\n        </dd>\n        <dt><span>First Name</span></dt>\n        <dd>{{user.firstName}}</dd>\n        <dt><span>Last Name</span></dt>\n        <dd>{{user.lastName}}</dd>\n        <dt><span>Email</span></dt>\n        <dd>{{user.email}}</dd>\n        <dt><span>Created By</span></dt>\n        <dd>{{user.createdBy}}</dd>\n        <dt><span>Created Date</span></dt>\n        <dd>{{user.createdDate | date:'dd/MM/yy HH:mm' }}</dd>\n        <dt><span>Last Modified By</span></dt>\n        <dd>{{user.lastModifiedBy}}</dd>\n        <dt><span>Last Modified Date</span></dt>\n        <dd>{{user.lastModifiedDate | date:'dd/MM/yy HH:mm'}}</dd>\n        <dt><span>Profiles</span></dt>\n        <dd>\n            <ul class=\"list-unstyled\">\n                <li *ngFor=\"let authority of user.authorities\">\n                    <span class=\"badge badge-info\">{{authority}}</span>\n                </li>\n            </ul>\n        </dd>\n    </dl>\n    <button type=\"submit\"\n            routerLink=\"/user-management\"\n            class=\"btn btn-info\">\n        <span class=\"fa fa-arrow-left\"></span>&nbsp;<span> Back</span>\n    </button>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/user-management/user-management-detail.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var UserMgmtDetailComponent = /** @class */ (function () {
    function UserMgmtDetailComponent(userService, route) {
        this.userService = userService;
        this.route = route;
    }
    UserMgmtDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.route.params.subscribe(function (params) {
            _this.load(params['login']);
        });
    };
    UserMgmtDetailComponent.prototype.load = function (login) {
        var _this = this;
        this.userService.find(login).subscribe(function (user) {
            _this.user = user;
        });
    };
    UserMgmtDetailComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    UserMgmtDetailComponent = __decorate([
        core_1.Component({
            selector: 'jhi-user-mgmt-detail',
            template: __webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management-detail.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof shared_1.UserService !== "undefined" && shared_1.UserService) === "function" && _a || Object, typeof (_b = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _b || Object])
    ], UserMgmtDetailComponent);
    return UserMgmtDetailComponent;
    var _a, _b;
}());
exports.UserMgmtDetailComponent = UserMgmtDetailComponent;
//# sourceMappingURL=user-management-detail.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/user-management/user-management-dialog.component.html":
/***/ (function(module, exports) {

module.exports = "<form name=\"editForm\" role=\"form\" novalidate (ngSubmit)=\"save()\" #editForm=\"ngForm\">\n\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\" id=\"myUserLabel\">\n            Create or edit a User</h4>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\"\n            (click)=\"clear()\">&times;</button>\n    </div>\n    <div class=\"modal-body\">\n        <jhi-alert-error></jhi-alert-error>\n        <div class=\"form-group\" [hidden]=\"!user.id\">\n            <label>ID</label>\n            <input type=\"text\" class=\"form-control\" name=\"id\"\n                   [(ngModel)]=\"user.id\" readonly>\n        </div>\n\n        <div class=\"form-group\">\n            <label class=\"form-control-label\">Login</label>\n            <input type=\"text\" class=\"form-control\" name=\"login\" #loginInput=\"ngModel\"\n                   [(ngModel)]=\"user.login\" required minlength=\"1\" maxlength=\"50\" pattern=\"^[_'.@A-Za-z0-9-]*$\">\n\n            <div *ngIf=\"loginInput.dirty && loginInput.invalid\">\n                <small class=\"form-text text-danger\"\n                   *ngIf=\"loginInput.errors.required\">\n                    This field is required.\n                </small>\n\n                <small class=\"form-text text-danger\"\n                   *ngIf=\"loginInput.errors.maxlength\"\n                  >\n                    This field cannot be longer than 50 characters.\n                </small>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"form-control-label\">First Name</label>\n            <input type=\"text\" class=\"form-control\" name=\"firstName\" #firstNameInput=\"ngModel\"\n                   [(ngModel)]=\"user.firstName\" maxlength=\"50\">\n\n            <div *ngIf=\"firstNameInput.dirty && firstNameInput.invalid\">\n                <small class=\"form-text text-danger\"\n                   *ngIf=\"firstNameInput.errors.maxlength\"\n                  >\n                    This field cannot be longer than 50 characters.\n                </small>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label>Last Name</label>\n            <input type=\"text\" class=\"form-control\" name=\"lastName\" #lastNameInput=\"ngModel\"\n                   [(ngModel)]=\"user.lastName\" maxlength=\"50\">\n\n            <div *ngIf=\"lastNameInput.dirty && lastNameInput.invalid\">\n                <small class=\"form-text text-danger\"\n                   *ngIf=\"lastNameInput.errors.maxlength\"\n                  >\n                    This field cannot be longer than 50 characters.\n                </small>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"form-control-label\">Email</label>\n            <input type=\"email\" class=\"form-control\" name=\"email\" #emailInput=\"ngModel\"\n                   [(ngModel)]=\"user.email\" minlength=\"5\" required maxlength=\"100\" email>\n\n            <div *ngIf=\"emailInput.dirty && emailInput.invalid\">\n                <small class=\"form-text text-danger\"\n                   *ngIf=\"emailInput.errors.required\">\n                    This field is required.\n                </small>\n\n                <small class=\"form-text text-danger\"\n                   *ngIf=\"emailInput.errors.maxlength\"\n                  >\n                    This field cannot be longer than 100 characters.\n                </small>\n\n                <small class=\"form-text text-danger\"\n                   *ngIf=\"emailInput.errors.minlength\"\n                  >\n                    This field is required to be at least 5 characters.\n                </small>\n\n                <small class=\"form-text text-danger\"\n                   *ngIf=\"emailInput.errors.email\">\n                    Your email is invalid.\n                 </small>\n            </div>\n        </div>\n        <div class=\"form-check\">\n            <label class=\"form-check-label\" for=\"activated\">\n                <input class=\"form-check-input\" [disabled]=\"user.id === null\" type=\"checkbox\" id=\"activated\" name=\"activated\" [(ngModel)]=\"user.activated\">\n                <span>Activated</span>\n            </label>\n        </div>\n\n        <div class=\"form-group\">\n            <label>Profiles</label>\n            <select class=\"form-control\" multiple name=\"authority\" [(ngModel)]=\"user.authorities\">\n                <option *ngFor=\"let authority of authorities\" [value]=\"authority\">{{authority}}</option>\n            </select>\n        </div>\n    </div>\n    <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\" (click)=\"clear()\">\n            <span class=\"fa fa-ban\"></span>&nbsp;<span\n           >Cancel</span>\n        </button>\n        <button type=\"submit\" [disabled]=\"editForm.form.invalid || isSaving\" class=\"btn btn-primary\">\n            <span class=\"fa fa-floppy-o\"></span>&nbsp;<span>Save</span>\n        </button>\n    </div>\n</form>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/user-management/user-management-dialog.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var user_modal_service_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-modal.service.ts");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var UserMgmtDialogComponent = /** @class */ (function () {
    function UserMgmtDialogComponent(activeModal, userService, eventManager) {
        this.activeModal = activeModal;
        this.userService = userService;
        this.eventManager = eventManager;
    }
    UserMgmtDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isSaving = false;
        this.authorities = [];
        this.userService.authorities().subscribe(function (authorities) {
            _this.authorities = authorities;
        });
    };
    UserMgmtDialogComponent.prototype.clear = function () {
        this.activeModal.dismiss('cancel');
    };
    UserMgmtDialogComponent.prototype.save = function () {
        var _this = this;
        this.isSaving = true;
        if (this.user.id !== null) {
            this.userService.update(this.user).subscribe(function (response) { return _this.onSaveSuccess(response); }, function () { return _this.onSaveError(); });
        }
        else {
            this.user.langKey = 'en';
            this.userService.create(this.user).subscribe(function (response) { return _this.onSaveSuccess(response); }, function () { return _this.onSaveError(); });
        }
    };
    UserMgmtDialogComponent.prototype.onSaveSuccess = function (result) {
        this.eventManager.broadcast({ name: 'userListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    };
    UserMgmtDialogComponent.prototype.onSaveError = function () {
        this.isSaving = false;
    };
    UserMgmtDialogComponent = __decorate([
        core_1.Component({
            selector: 'jhi-user-mgmt-dialog',
            template: __webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management-dialog.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof ng_bootstrap_1.NgbActiveModal !== "undefined" && ng_bootstrap_1.NgbActiveModal) === "function" && _a || Object, typeof (_b = typeof shared_1.UserService !== "undefined" && shared_1.UserService) === "function" && _b || Object, typeof (_c = typeof ng_jhipster_1.JhiEventManager !== "undefined" && ng_jhipster_1.JhiEventManager) === "function" && _c || Object])
    ], UserMgmtDialogComponent);
    return UserMgmtDialogComponent;
    var _a, _b, _c;
}());
exports.UserMgmtDialogComponent = UserMgmtDialogComponent;
var UserDialogComponent = /** @class */ (function () {
    function UserDialogComponent(route, userModalService) {
        this.route = route;
        this.userModalService = userModalService;
    }
    UserDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeSub = this.route.params.subscribe(function (params) {
            if (params['login']) {
                _this.userModalService.open(UserMgmtDialogComponent, params['login']);
            }
            else {
                _this.userModalService.open(UserMgmtDialogComponent);
            }
        });
    };
    UserDialogComponent.prototype.ngOnDestroy = function () {
        this.routeSub.unsubscribe();
    };
    UserDialogComponent = __decorate([
        core_1.Component({
            selector: 'jhi-user-dialog',
            template: ''
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _a || Object, typeof (_b = typeof user_modal_service_1.UserModalService !== "undefined" && user_modal_service_1.UserModalService) === "function" && _b || Object])
    ], UserDialogComponent);
    return UserDialogComponent;
    var _a, _b;
}());
exports.UserDialogComponent = UserDialogComponent;
//# sourceMappingURL=user-management-dialog.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/user-management/user-management.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <h2>\n        <span>Users</span>\n        <button class=\"btn btn-primary float-right jh-create-entity\" [routerLink]=\"['/', { outlets: { popup: ['user-management-new'] } }]\">\n            <span class=\"fa fa-plus\"></span> <span>Create a new User</span>\n        </button>\n    </h2>\n    <jhi-alert></jhi-alert>\n    <div class=\"table-responsive\" *ngIf=\"users\">\n        <table class=\"table table-striped\">\n            <thead>\n            <tr jhiSort [(predicate)]=\"predicate\" [(ascending)]=\"reverse\" [callback]=\"transition.bind(this)\">\n                <th jhiSortBy=\"id\"><span>ID</span> <span class=\"fa fa-sort\"></span></th>\n                <th jhiSortBy=\"login\"><span>Login</span> <span class=\"fa fa-sort\"></span></th>\n                <th jhiSortBy=\"email\"><span>Email</span> <span class=\"fa fa-sort\"></span></th>\n                <th></th>\n                <th><span>Profiles</span></th>\n                <th jhiSortBy=\"createdDate\"><span>Created Date</span> <span class=\"fa fa-sort\"></span></th>\n                <th jhiSortBy=\"lastModifiedBy\"><span>Last Modified By</span> <span class=\"fa fa-sort\"></span></th>\n                <th jhiSortBy=\"lastModifiedDate\"><span>Last Modified Date</span> <span class=\"fa fa-sort\"></span></th>\n                <th></th>\n            </tr>\n            </thead>\n            <tbody *ngIf =\"users\">\n            <tr *ngFor=\"let user of users; trackBy: trackIdentity\">\n                <td><a [routerLink]=\"['../user-management', user.login]\">{{user.id}}</a></td>\n                <td>{{user.login}}</td>\n                <td>{{user.email}}</td>\n                <td>\n                    <button class=\"btn btn-danger btn-sm\" (click)=\"setActive(user, true)\" *ngIf=\"!user.activated\"\n                           >Deactivated</button>\n                    <button class=\"btn btn-success btn-sm\" (click)=\"setActive(user, false)\" *ngIf=\"user.activated\"\n                            [disabled]=\"currentAccount.login === user.login\">Activated</button>\n                </td>\n                \n                <td>\n                    <div *ngFor=\"let authority of user.authorities\">\n                        <span class=\"badge badge-info\">{{ authority }}</span>\n                    </div>\n                </td>\n                <td>{{user.createdDate | date:'dd/MM/yy HH:mm'}}</td>\n                <td>{{user.lastModifiedBy}}</td>\n                <td>{{user.lastModifiedDate | date:'dd/MM/yy HH:mm'}}</td>\n                <td class=\"text-right\">\n                    <div class=\"btn-group flex-btn-group-container\">\n                        <button type=\"submit\"\n                                [routerLink]=\"['../user-management', user.login]\"\n                                class=\"btn btn-info btn-sm\">\n                            <span class=\"fa fa-eye\"></span>\n                            <span class=\"d-none d-md-inline\">View</span>\n                        </button>\n                        <button type=\"submit\"\n                                [routerLink]=\"['/', { outlets: { popup: 'user-management/'+ user.login + '/edit'} }]\"\n                                replaceUrl=\"true\"\n                                class=\"btn btn-primary btn-sm\">\n                            <span class=\"fa fa-pencil\"></span>\n                            <span class=\"d-none d-md-inline\">Edit</span>\n                        </button>\n                        <button type=\"submit\"\n                                [routerLink]=\"['/', { outlets: { popup: 'user-management/'+ user.login + '/delete'} }]\"\n                                replaceUrl=\"true\"\n                                class=\"btn btn-danger btn-sm\" [disabled]=\"currentAccount.login === user.login\">\n                            <span class=\"fa fa-remove\"></span>\n                            <span class=\"d-none d-md-inline\">Delete</span>\n                        </button>\n                    </div>\n                </td>\n            </tr>\n            </tbody>\n        </table>\n    </div>\n    <div *ngIf=\"users\">\n        <div class=\"row justify-content-center\">\n            <jhi-item-count [page]=\"page\" [total]=\"queryCount\" [itemsPerPage]=\"itemsPerPage\"></jhi-item-count>\n        </div>\n        <div class=\"row justify-content-center\">\n            <ngb-pagination [collectionSize]=\"totalItems\" [pageSize]=\"itemsPerPage\" [(page)]=\"page\" (pageChange)=\"loadPage(page)\"></ngb-pagination>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/user-management/user-management.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var UserMgmtComponent = /** @class */ (function () {
    function UserMgmtComponent(userService, parseLinks, alertService, principal, eventManager, activatedRoute, router) {
        var _this = this;
        this.userService = userService;
        this.parseLinks = parseLinks;
        this.alertService = alertService;
        this.principal = principal;
        this.eventManager = eventManager;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.itemsPerPage = shared_1.ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(function (data) {
            _this.page = data['pagingParams'].page;
            _this.previousPage = data['pagingParams'].page;
            _this.reverse = data['pagingParams'].ascending;
            _this.predicate = data['pagingParams'].predicate;
        });
    }
    UserMgmtComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.principal.identity().then(function (account) {
            _this.currentAccount = account;
            _this.loadAll();
            _this.registerChangeInUsers();
        });
    };
    UserMgmtComponent.prototype.ngOnDestroy = function () {
        this.routeData.unsubscribe();
    };
    UserMgmtComponent.prototype.registerChangeInUsers = function () {
        var _this = this;
        this.eventManager.subscribe('userListModification', function (response) { return _this.loadAll(); });
    };
    UserMgmtComponent.prototype.setActive = function (user, isActivated) {
        var _this = this;
        user.activated = isActivated;
        this.userService.update(user).subscribe(function (response) {
            if (response.status === 200) {
                _this.error = null;
                _this.success = 'OK';
                _this.loadAll();
            }
            else {
                _this.success = null;
                _this.error = 'ERROR';
            }
        });
    };
    UserMgmtComponent.prototype.loadAll = function () {
        var _this = this;
        this.userService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(function (res) { return _this.onSuccess(res.json, res.headers); }, function (res) { return _this.onError(res.json); });
    };
    UserMgmtComponent.prototype.trackIdentity = function (index, item) {
        return item.id;
    };
    UserMgmtComponent.prototype.sort = function () {
        var result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    };
    UserMgmtComponent.prototype.loadPage = function (page) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    };
    UserMgmtComponent.prototype.transition = function () {
        this.router.navigate(['/user-management'], {
            queryParams: {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    };
    UserMgmtComponent.prototype.onSuccess = function (data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.users = data;
    };
    UserMgmtComponent.prototype.onError = function (error) {
        this.alertService.error(error.error, error.message, null);
    };
    UserMgmtComponent = __decorate([
        core_1.Component({
            selector: 'jhi-user-mgmt',
            template: __webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof shared_1.UserService !== "undefined" && shared_1.UserService) === "function" && _a || Object, typeof (_b = typeof ng_jhipster_1.JhiParseLinks !== "undefined" && ng_jhipster_1.JhiParseLinks) === "function" && _b || Object, typeof (_c = typeof ng_jhipster_1.JhiAlertService !== "undefined" && ng_jhipster_1.JhiAlertService) === "function" && _c || Object, typeof (_d = typeof shared_1.Principal !== "undefined" && shared_1.Principal) === "function" && _d || Object, typeof (_e = typeof ng_jhipster_1.JhiEventManager !== "undefined" && ng_jhipster_1.JhiEventManager) === "function" && _e || Object, typeof (_f = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _f || Object, typeof (_g = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _g || Object])
    ], UserMgmtComponent);
    return UserMgmtComponent;
    var _a, _b, _c, _d, _e, _f, _g;
}());
exports.UserMgmtComponent = UserMgmtComponent;
//# sourceMappingURL=user-management.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/user-management/user-management.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var user_management_component_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management.component.ts");
var user_management_detail_component_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management-detail.component.ts");
var user_management_dialog_component_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management-dialog.component.ts");
var user_management_delete_dialog_component_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/user-management/user-management-delete-dialog.component.ts");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var UserResolve = /** @class */ (function () {
    function UserResolve(principal) {
        this.principal = principal;
    }
    UserResolve.prototype.canActivate = function () {
        var _this = this;
        return this.principal.identity().then(function (account) { return _this.principal.hasAnyAuthority(['ROLE_ADMIN']); });
    };
    UserResolve = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof shared_1.Principal !== "undefined" && shared_1.Principal) === "function" && _a || Object])
    ], UserResolve);
    return UserResolve;
    var _a;
}());
exports.UserResolve = UserResolve;
var UserResolvePagingParams = /** @class */ (function () {
    function UserResolvePagingParams(paginationUtil) {
        this.paginationUtil = paginationUtil;
    }
    UserResolvePagingParams.prototype.resolve = function (route, state) {
        var page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        var sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    };
    UserResolvePagingParams = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof ng_jhipster_1.JhiPaginationUtil !== "undefined" && ng_jhipster_1.JhiPaginationUtil) === "function" && _a || Object])
    ], UserResolvePagingParams);
    return UserResolvePagingParams;
    var _a;
}());
exports.UserResolvePagingParams = UserResolvePagingParams;
exports.userMgmtRoute = [
    {
        path: 'user-management',
        component: user_management_component_1.UserMgmtComponent,
        resolve: {
            'pagingParams': UserResolvePagingParams
        },
        data: {
            pageTitle: 'Users'
        }
    },
    {
        path: 'user-management/:login',
        component: user_management_detail_component_1.UserMgmtDetailComponent,
        data: {
            pageTitle: 'Users'
        }
    }
];
exports.userDialogRoute = [
    {
        path: 'user-management-new',
        component: user_management_dialog_component_1.UserDialogComponent,
        outlet: 'popup'
    },
    {
        path: 'user-management/:login/edit',
        component: user_management_dialog_component_1.UserDialogComponent,
        outlet: 'popup'
    },
    {
        path: 'user-management/:login/delete',
        component: user_management_delete_dialog_component_1.UserDeleteDialogComponent,
        outlet: 'popup'
    }
];
//# sourceMappingURL=user-management.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/admin/user-management/user-modal.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var UserModalService = /** @class */ (function () {
    function UserModalService(modalService, router, userService) {
        this.modalService = modalService;
        this.router = router;
        this.userService = userService;
        this.ngbModalRef = null;
    }
    UserModalService.prototype.open = function (component, id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var isOpen = _this.ngbModalRef !== null;
            if (isOpen) {
                resolve(_this.ngbModalRef);
            }
            if (id) {
                _this.userService.find(id).subscribe(function (user) {
                    _this.ngbModalRef = _this.userModalRef(component, user);
                    resolve(_this.ngbModalRef);
                });
            }
            else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(function () {
                    _this.ngbModalRef = _this.userModalRef(component, new shared_1.User());
                    resolve(_this.ngbModalRef);
                }, 0);
            }
        });
    };
    UserModalService.prototype.userModalRef = function (component, user) {
        var _this = this;
        var modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.user = user;
        modalRef.result.then(function (result) {
            _this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true });
            _this.ngbModalRef = null;
        }, function (reason) {
            _this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true });
            _this.ngbModalRef = null;
        });
        return modalRef;
    };
    UserModalService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof ng_bootstrap_1.NgbModal !== "undefined" && ng_bootstrap_1.NgbModal) === "function" && _a || Object, typeof (_b = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _b || Object, typeof (_c = typeof shared_1.UserService !== "undefined" && shared_1.UserService) === "function" && _c || Object])
    ], UserModalService);
    return UserModalService;
    var _a, _b, _c;
}());
exports.UserModalService = UserModalService;
//# sourceMappingURL=user-modal.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/app-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var layouts_1 = __webpack_require__("../../../../../src/main/webapp/app/layouts/index.ts");
var LAYOUT_ROUTES = [
    layouts_1.navbarRoute
].concat(layouts_1.errorRoute);
var KataSgAppRoutingModule = /** @class */ (function () {
    function KataSgAppRoutingModule() {
    }
    KataSgAppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forRoot(LAYOUT_ROUTES, { useHash: true })
            ],
            exports: [
                router_1.RouterModule
            ]
        })
    ], KataSgAppRoutingModule);
    return KataSgAppRoutingModule;
}());
exports.KataSgAppRoutingModule = KataSgAppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/app.constants.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// DO NOT EDIT THIS FILE, EDIT THE WEBPACK COMMON CONFIG INSTEAD, WHICH WILL MODIFY THIS FILE
/* tslint:disable */
var _VERSION = '0.0.1'; // This value will be overwritten by Webpack
var _DEBUG_INFO_ENABLED = true; // This value will be overwritten by Webpack
var _SERVER_API_URL = ''; // This value will be overwritten by Webpack
/* @toreplace VERSION */
/* @toreplace DEBUG_INFO_ENABLED */
/* @toreplace SERVER_API_URL */
/* tslint:enable */
exports.VERSION = _VERSION;
exports.DEBUG_INFO_ENABLED = _DEBUG_INFO_ENABLED;
exports.SERVER_API_URL = _SERVER_API_URL;
//# sourceMappingURL=app.constants.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/app.main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_dynamic_1 = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
var prod_config_1 = __webpack_require__("../../../../../src/main/webapp/app/blocks/config/prod.config.ts");
var app_module_1 = __webpack_require__("../../../../../src/main/webapp/app/app.module.ts");
prod_config_1.ProdConfig();
if (false) {
    module['hot'].accept();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.KataSgAppModule)
    .then(function (success) { return console.log("Application started"); })
    .catch(function (err) { return console.error(err); });
//# sourceMappingURL=app.main.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__("../../../../../src/main/webapp/app/vendor.ts");
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var platform_browser_1 = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
var ng2_webstorage_1 = __webpack_require__("../../../../ng2-webstorage/dist/app.js");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var app_routing_module_1 = __webpack_require__("../../../../../src/main/webapp/app/app-routing.module.ts");
var home_module_1 = __webpack_require__("../../../../../src/main/webapp/app/home/home.module.ts");
var admin_module_1 = __webpack_require__("../../../../../src/main/webapp/app/admin/admin.module.ts");
var account_module_1 = __webpack_require__("../../../../../src/main/webapp/app/account/account.module.ts");
var entity_module_1 = __webpack_require__("../../../../../src/main/webapp/app/entities/entity.module.ts");
var http_provider_1 = __webpack_require__("../../../../../src/main/webapp/app/blocks/interceptor/http.provider.ts");
var uib_pagination_config_1 = __webpack_require__("../../../../../src/main/webapp/app/blocks/config/uib-pagination.config.ts");
// jhipster-needle-angular-add-module-import JHipster will add new module here
var layouts_1 = __webpack_require__("../../../../../src/main/webapp/app/layouts/index.ts");
var KataSgAppModule = /** @class */ (function () {
    function KataSgAppModule() {
    }
    KataSgAppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_module_1.KataSgAppRoutingModule,
                ng2_webstorage_1.Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
                shared_1.KataSgSharedModule,
                home_module_1.KataSgHomeModule,
                admin_module_1.KataSgAdminModule,
                account_module_1.KataSgAccountModule,
                entity_module_1.KataSgEntityModule,
            ],
            declarations: [
                layouts_1.JhiMainComponent,
                layouts_1.NavbarComponent,
                layouts_1.ErrorComponent,
                layouts_1.PageRibbonComponent,
                layouts_1.FooterComponent
            ],
            providers: [
                layouts_1.ProfileService,
                http_provider_1.customHttpProvider(),
                uib_pagination_config_1.PaginationConfig,
                shared_1.UserRouteAccessService
            ],
            bootstrap: [layouts_1.JhiMainComponent]
        })
    ], KataSgAppModule);
    return KataSgAppModule;
}());
exports.KataSgAppModule = KataSgAppModule;
//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/blocks/config/prod.config.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var app_constants_1 = __webpack_require__("../../../../../src/main/webapp/app/app.constants.ts");
function ProdConfig() {
    // disable debug data on prod profile to improve performance
    if (!app_constants_1.DEBUG_INFO_ENABLED) {
        core_1.enableProdMode();
    }
}
exports.ProdConfig = ProdConfig;
//# sourceMappingURL=prod.config.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/blocks/config/uib-pagination.config.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var PaginationConfig = /** @class */ (function () {
    // tslint:disable-next-line: no-unused-variable
    function PaginationConfig(config) {
        this.config = config;
        config.boundaryLinks = true;
        config.maxSize = 5;
        config.pageSize = shared_1.ITEMS_PER_PAGE;
        config.size = 'sm';
    }
    PaginationConfig = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof ng_bootstrap_1.NgbPaginationConfig !== "undefined" && ng_bootstrap_1.NgbPaginationConfig) === "function" && _a || Object])
    ], PaginationConfig);
    return PaginationConfig;
    var _a;
}());
exports.PaginationConfig = PaginationConfig;
//# sourceMappingURL=uib-pagination.config.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/blocks/interceptor/auth-expired.interceptor.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var Observable_1 = __webpack_require__("../../../../rxjs/Observable.js");
var login_service_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/login/login.service.ts");
var AuthExpiredInterceptor = /** @class */ (function (_super) {
    __extends(AuthExpiredInterceptor, _super);
    function AuthExpiredInterceptor(injector) {
        var _this = _super.call(this) || this;
        _this.injector = injector;
        return _this;
    }
    AuthExpiredInterceptor.prototype.requestIntercept = function (options) {
        return options;
    };
    AuthExpiredInterceptor.prototype.responseIntercept = function (observable) {
        var _this = this;
        return observable.catch(function (error, source) {
            if (error.status === 401) {
                var loginService = _this.injector.get(login_service_1.LoginService);
                loginService.logout();
            }
            return Observable_1.Observable.throw(error);
        });
    };
    return AuthExpiredInterceptor;
}(ng_jhipster_1.JhiHttpInterceptor));
exports.AuthExpiredInterceptor = AuthExpiredInterceptor;
//# sourceMappingURL=auth-expired.interceptor.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/blocks/interceptor/auth.interceptor.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var AuthInterceptor = /** @class */ (function (_super) {
    __extends(AuthInterceptor, _super);
    function AuthInterceptor(localStorage, sessionStorage) {
        var _this = _super.call(this) || this;
        _this.localStorage = localStorage;
        _this.sessionStorage = sessionStorage;
        return _this;
    }
    AuthInterceptor.prototype.requestIntercept = function (options) {
        if (!options || !options.url || /^http/.test(options.url)) {
            return options;
        }
        var token = this.localStorage.retrieve('authenticationToken') || this.sessionStorage.retrieve('authenticationToken');
        if (!!token) {
            options.headers.append('Authorization', 'Bearer ' + token);
        }
        return options;
    };
    AuthInterceptor.prototype.responseIntercept = function (observable) {
        return observable; // by pass
    };
    return AuthInterceptor;
}(ng_jhipster_1.JhiHttpInterceptor));
exports.AuthInterceptor = AuthInterceptor;
//# sourceMappingURL=auth.interceptor.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/blocks/interceptor/errorhandler.interceptor.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var Observable_1 = __webpack_require__("../../../../rxjs/Observable.js");
var ErrorHandlerInterceptor = /** @class */ (function (_super) {
    __extends(ErrorHandlerInterceptor, _super);
    function ErrorHandlerInterceptor(eventManager) {
        var _this = _super.call(this) || this;
        _this.eventManager = eventManager;
        return _this;
    }
    ErrorHandlerInterceptor.prototype.requestIntercept = function (options) {
        return options;
    };
    ErrorHandlerInterceptor.prototype.responseIntercept = function (observable) {
        var _this = this;
        return observable.catch(function (error) {
            if (!(error.status === 401 && (error.text() === '' ||
                (error.json().path && error.json().path.indexOf('/api/account') === 0)))) {
                if (_this.eventManager !== undefined) {
                    _this.eventManager.broadcast({ name: 'kataSgApp.httpError', content: error });
                }
            }
            return Observable_1.Observable.throw(error);
        });
    };
    return ErrorHandlerInterceptor;
}(ng_jhipster_1.JhiHttpInterceptor));
exports.ErrorHandlerInterceptor = ErrorHandlerInterceptor;
//# sourceMappingURL=errorhandler.interceptor.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/blocks/interceptor/http.provider.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var auth_interceptor_1 = __webpack_require__("../../../../../src/main/webapp/app/blocks/interceptor/auth.interceptor.ts");
var ng2_webstorage_1 = __webpack_require__("../../../../ng2-webstorage/dist/app.js");
var auth_expired_interceptor_1 = __webpack_require__("../../../../../src/main/webapp/app/blocks/interceptor/auth-expired.interceptor.ts");
var errorhandler_interceptor_1 = __webpack_require__("../../../../../src/main/webapp/app/blocks/interceptor/errorhandler.interceptor.ts");
var notification_interceptor_1 = __webpack_require__("../../../../../src/main/webapp/app/blocks/interceptor/notification.interceptor.ts");
function interceptableFactory(backend, defaultOptions, localStorage, sessionStorage, injector, eventManager) {
    return new ng_jhipster_1.JhiInterceptableHttp(backend, defaultOptions, [
        new auth_interceptor_1.AuthInterceptor(localStorage, sessionStorage),
        new auth_expired_interceptor_1.AuthExpiredInterceptor(injector),
        // Other interceptors can be added here
        new errorhandler_interceptor_1.ErrorHandlerInterceptor(eventManager),
        new notification_interceptor_1.NotificationInterceptor(injector)
    ]);
}
exports.interceptableFactory = interceptableFactory;
function customHttpProvider() {
    return {
        provide: http_1.Http,
        useFactory: interceptableFactory,
        deps: [
            http_1.XHRBackend,
            http_1.RequestOptions,
            ng2_webstorage_1.LocalStorageService,
            ng2_webstorage_1.SessionStorageService,
            core_1.Injector,
            ng_jhipster_1.JhiEventManager
        ]
    };
}
exports.customHttpProvider = customHttpProvider;
//# sourceMappingURL=http.provider.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/blocks/interceptor/notification.interceptor.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var Observable_1 = __webpack_require__("../../../../rxjs/Observable.js");
var NotificationInterceptor = /** @class */ (function (_super) {
    __extends(NotificationInterceptor, _super);
    // tslint:disable-next-line: no-unused-variable
    function NotificationInterceptor(injector) {
        var _this = _super.call(this) || this;
        _this.injector = injector;
        setTimeout(function () { return _this.alertService = injector.get(ng_jhipster_1.JhiAlertService); });
        return _this;
    }
    NotificationInterceptor.prototype.requestIntercept = function (options) {
        return options;
    };
    NotificationInterceptor.prototype.responseIntercept = function (observable) {
        var _this = this;
        return observable.map(function (response) {
            var headers = [];
            response.headers.forEach(function (value, name) {
                if (name.toLowerCase().endsWith('app-alert') || name.toLowerCase().endsWith('app-params')) {
                    headers.push(name);
                }
            });
            if (headers.length > 1) {
                headers.sort();
                var alertKey = response.headers.get(headers[0]);
                if (typeof alertKey === 'string') {
                    if (_this.alertService) {
                        _this.alertService.success(alertKey, null, null);
                    }
                }
            }
            return response;
        }).catch(function (error) {
            return Observable_1.Observable.throw(error); // here, response is an error
        });
    };
    return NotificationInterceptor;
}(ng_jhipster_1.JhiHttpInterceptor));
exports.NotificationInterceptor = NotificationInterceptor;
//# sourceMappingURL=notification.interceptor.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/entities/entity.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */
var KataSgEntityModule = /** @class */ (function () {
    function KataSgEntityModule() {
    }
    KataSgEntityModule = __decorate([
        core_1.NgModule({
            imports: [],
            declarations: [],
            entryComponents: [],
            providers: [],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        })
    ], KataSgEntityModule);
    return KataSgEntityModule;
}());
exports.KataSgEntityModule = KataSgEntityModule;
//# sourceMappingURL=entity.module.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "    <div class=\"row\">\n        <div class=\"col-md-3\">\n            <span class=\"hipster img-fluid rounded\"></span>\n        </div>\n        <div class=\"col-md-9\">\n            <h1 class=\"display-4\">Welcome, To Société Générale</h1>\n           \n\n            <div [ngSwitch]=\"isAuthenticated()\">\n               \n                <div class=\"alert alert-success\" *ngSwitchCase=\"true\">\n                    <span *ngIf=\"account\"\n                       > You are logged in as user <b>\"{{account.firstName}} {{account.lastName}}\"</b>. </span>\n\n                       <br>\n                       <span *ngIf=\"account\"\n                       > Your Bank Account Balance <b>\"{{compte.amount}}\"</b>. </span> \n                </div>\n\n                <div class=\"alert alert-warning\" *ngSwitchCase=\"false\">\n                    <span>If you want to </span>\n                    <a class=\"alert-link\" (click)=\"login()\">sign in</a><span>\n                        <!--, you can try the default accounts:<br/> \n                         user (login=\"jpierre\" and password=\"user\") <br/>- User (login=\"sqli\" and password=\"user\"). -->\n                    </span>\n                </div>\n                <div class=\"alert alert-warning\" *ngSwitchCase=\"false\">\n                    <span>You don't have an account yet?</span>\n                    <a class=\"alert-link\" routerLink=\"register\">Register a new account</a>\n                </div>\n                <br><br>\n             <!--    <div>\n                    Montant de retrait <input type=\"text\" name=\"\">\n                    <button type=\"submit\">Valider</button>\n\n                    <button mat-raised-button color=\"primary\" (click)=\"chercher()\">Recherche</button> \n                </div> -->\n\n\n      <div *ngSwitchCase=\"true\">\n                      <div class=\"form-group\">\n                        <span *ngIf=\"operationFlag\"> Withdraw successfully completed, your  new Balance is : <b>{{compte.amount}}</b>. </span> \n                        <br>\n                        <label class=\"control-label\">Amount to withdraw:</label> \n                        <input class=\"form-control\" type=\"text\" [(ngModel)]=\"updateamount\" >\n                      </div>\n                      <button class=\"btn btn-primary\" (click)=\"save()\"> Submit</button>\n                </div>\n              \n            </div>\n      \n        </div>\n    </div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/home/home.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(principal, loginModalService, eventManager, CompteService) {
        this.principal = principal;
        this.loginModalService = loginModalService;
        this.eventManager = eventManager;
        this.CompteService = CompteService;
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.principal.identity().then(function (account) {
            _this.account = account;
        });
        this.getBalance();
        this.registerAuthenticationSuccess();
    };
    HomeComponent.prototype.getBalance = function () {
        var _this = this;
        this.CompteService.getCompte().subscribe(function (data) {
            _this.compte = data;
        });
    };
    HomeComponent.prototype.registerAuthenticationSuccess = function () {
        var _this = this;
        this.eventManager.subscribe('authenticationSuccess', function (message) {
            _this.principal.identity().then(function (account) {
                _this.account = account;
            });
        });
    };
    HomeComponent.prototype.isAuthenticated = function () {
        return this.principal.isAuthenticated();
    };
    HomeComponent.prototype.login = function () {
        this.modalRef = this.loginModalService.open();
    };
    HomeComponent.prototype.save = function () {
        var _this = this;
        debugger;
        /*this.compte.amount = this.compte.amount - this.updateamount;*/
        this.CompteService.update(this.updateamount).subscribe(function (response) {
            if (response.status === 200) {
                _this.compte = response.json;
                _this.operationFlag = true;
            }
            else {
                _this.operationFlag = false;
            }
        });
        this.updateamount = '';
        //  this.getBalance();
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'jhi-home',
            template: __webpack_require__("../../../../../src/main/webapp/app/home/home.component.html"),
            styles: [__webpack_require__("../../../../../src/main/webapp/app/home/home.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof shared_1.Principal !== "undefined" && shared_1.Principal) === "function" && _a || Object, typeof (_b = typeof shared_1.LoginModalService !== "undefined" && shared_1.LoginModalService) === "function" && _b || Object, typeof (_c = typeof ng_jhipster_1.JhiEventManager !== "undefined" && ng_jhipster_1.JhiEventManager) === "function" && _c || Object, typeof (_d = typeof shared_1.CompteService !== "undefined" && shared_1.CompteService) === "function" && _d || Object])
    ], HomeComponent);
    return HomeComponent;
    var _a, _b, _c, _d;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/home/home.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* ==========================================================================\nMain page styles\n========================================================================== */\n\n.hipster {\n    display: inline-block;\n    width: 347px;\n    height: 497px;\n    background: url(" + __webpack_require__("../../../../../src/main/webapp/content/images/hipster.png") + ") no-repeat center top;\n    background-size: contain;\n}\n\n/* wait autoprefixer update to allow simple generation of high pixel density media query */\n@media\nonly screen and (-webkit-min-device-pixel-ratio: 2),\nonly screen and (   min--moz-device-pixel-ratio: 2),\nonly screen and (        min-device-pixel-ratio: 2),\nonly screen and (                -webkit-min-device-pixel-ratio: 2),\nonly screen and (                min-resolution: 192dpi),\nonly screen and (                min-resolution: 2dppx) {\n    .hipster {\n        background: url(" + __webpack_require__("../../../../../src/main/webapp/content/images/hipster2x.png") + ") no-repeat center top;\n        background-size: contain;\n    }\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/main/webapp/app/home/home.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var _1 = __webpack_require__("../../../../../src/main/webapp/app/home/index.ts");
var KataSgHomeModule = /** @class */ (function () {
    function KataSgHomeModule() {
    }
    KataSgHomeModule = __decorate([
        core_1.NgModule({
            imports: [
                shared_1.KataSgSharedModule,
                router_1.RouterModule.forChild([_1.HOME_ROUTE])
            ],
            declarations: [
                _1.HomeComponent,
            ],
            entryComponents: [],
            providers: [],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        })
    ], KataSgHomeModule);
    return KataSgHomeModule;
}());
exports.KataSgHomeModule = KataSgHomeModule;
//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/home/home.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var _1 = __webpack_require__("../../../../../src/main/webapp/app/home/index.ts");
exports.HOME_ROUTE = {
    path: '',
    component: _1.HomeComponent,
    data: {
        authorities: [],
        pageTitle: 'Welcome, Java Hipster!'
    }
};
//# sourceMappingURL=home.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/home/index.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__("../../../../../src/main/webapp/app/home/home.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/home/home.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/home/home.module.ts"));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/error/error.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <div class=\"row\">\n        <div class=\"col-md-4\">\n            <span class=\"hipster img-fluid rounded\"></span>\n        </div>\n        <div class=\"col-md-8\">\n            <h1>Error Page!</h1>\n\n            <div [hidden]=\"!errorMessage\">\n                <div class=\"alert alert-danger\">{{errorMessage}}\n                </div>\n            </div>\n            <div [hidden]=\"!error403\" class=\"alert alert-danger\">You are not authorized to access this page.\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/error/error.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var ErrorComponent = /** @class */ (function () {
    function ErrorComponent(route) {
        this.route = route;
    }
    ErrorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            if (routeData.error403) {
                _this.error403 = routeData.error403;
            }
            if (routeData.errorMessage) {
                _this.errorMessage = routeData.errorMessage;
            }
        });
    };
    ErrorComponent = __decorate([
        core_1.Component({
            selector: 'jhi-error',
            template: __webpack_require__("../../../../../src/main/webapp/app/layouts/error/error.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _a || Object])
    ], ErrorComponent);
    return ErrorComponent;
    var _a;
}());
exports.ErrorComponent = ErrorComponent;
//# sourceMappingURL=error.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/error/error.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var error_component_1 = __webpack_require__("../../../../../src/main/webapp/app/layouts/error/error.component.ts");
exports.errorRoute = [
    {
        path: 'error',
        component: error_component_1.ErrorComponent,
        data: {
            authorities: [],
            pageTitle: 'Error page!'
        },
    },
    {
        path: 'accessdenied',
        component: error_component_1.ErrorComponent,
        data: {
            authorities: [],
            pageTitle: 'Error page!',
            error403: true
        },
    }
];
//# sourceMappingURL=error.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"footer\">\n    <p>This is your footer</p>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/footer/footer.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent = __decorate([
        core_1.Component({
            selector: 'jhi-footer',
            template: __webpack_require__("../../../../../src/main/webapp/app/layouts/footer/footer.component.html")
        })
    ], FooterComponent);
    return FooterComponent;
}());
exports.FooterComponent = FooterComponent;
//# sourceMappingURL=footer.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/index.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__("../../../../../src/main/webapp/app/layouts/error/error.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/layouts/error/error.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/layouts/main/main.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/layouts/footer/footer.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/layouts/navbar/navbar.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/layouts/navbar/navbar.route.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/layouts/profiles/page-ribbon.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/layouts/profiles/profile.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/layouts/profiles/profile-info.model.ts"));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/main/main.component.html":
/***/ (function(module, exports) {

module.exports = "<jhi-page-ribbon></jhi-page-ribbon>\n<div>\n    <router-outlet name=\"navbar\"></router-outlet>\n</div>\n<div class=\"container-fluid\">\n    <div class=\"card jh-card\">\n        <router-outlet></router-outlet>\n        <router-outlet name=\"popup\"></router-outlet>\n    </div>\n    <jhi-footer></jhi-footer>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/main/main.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var platform_browser_1 = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
var JhiMainComponent = /** @class */ (function () {
    function JhiMainComponent(titleService, router) {
        this.titleService = titleService;
        this.router = router;
    }
    JhiMainComponent.prototype.getPageTitle = function (routeSnapshot) {
        var title = (routeSnapshot.data && routeSnapshot.data['pageTitle']) ? routeSnapshot.data['pageTitle'] : 'kataSgApp';
        if (routeSnapshot.firstChild) {
            title = this.getPageTitle(routeSnapshot.firstChild) || title;
        }
        return title;
    };
    JhiMainComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationEnd) {
                _this.titleService.setTitle(_this.getPageTitle(_this.router.routerState.snapshot.root));
            }
        });
    };
    JhiMainComponent = __decorate([
        core_1.Component({
            selector: 'jhi-main',
            template: __webpack_require__("../../../../../src/main/webapp/app/layouts/main/main.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof platform_browser_1.Title !== "undefined" && platform_browser_1.Title) === "function" && _a || Object, typeof (_b = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _b || Object])
    ], JhiMainComponent);
    return JhiMainComponent;
    var _a, _b;
}());
exports.JhiMainComponent = JhiMainComponent;
//# sourceMappingURL=main.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-dark navbar-expand-md jh-navbar\">\n    <div class=\"jh-logo-container float-left\">\n        <a class=\"jh-navbar-toggler d-lg-none float-right\" href=\"javascript:void(0);\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\" (click)=\"toggleNavbar()\">\n            <i class=\"fa fa-bars\"></i>\n        </a>\n        <a class=\"navbar-brand logo float-left\" routerLink=\"/\" (click)=\"collapseNavbar()\">\n            <span class=\"logo-img\"></span>\n            <span class=\"navbar-title\">KataSG</span> <span class=\"navbar-version\">{{version}}</span>\n        </a>\n    </div>\n    <div class=\"navbar-collapse collapse\" id=\"navbarResponsive\" [ngbCollapse]=\"isNavbarCollapsed\" [ngSwitch]=\"isAuthenticated()\">\n        <ul class=\"navbar-nav ml-auto\">\n            <li class=\"nav-item\" routerLinkActive=\"active\" [routerLinkActiveOptions]=\"{exact: true}\">\n                <a class=\"nav-link\" routerLink=\"/\" (click)=\"collapseNavbar()\">\n                    <span>\n                        <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                        <span>Home</span>\n                    </span>\n                </a>\n            </li>\n            <!-- jhipster-needle-add-element-to-menu - JHipster will add new menu items here -->\n<!--             <li *ngSwitchCase=\"true\" ngbDropdown class=\"nav-item dropdown pointer\" routerLinkActive=\"active\" [routerLinkActiveOptions]=\"{exact: true}\">\n                <a class=\"nav-link dropdown-toggle\" ngbDropdownToggle href=\"javascript:void(0);\" id=\"entity-menu\">\n                    <span>\n                        <i class=\"fa fa-th-list\" aria-hidden=\"true\"></i>\n                        <span>\n                            Entities\n                        </span>\n                    </span>\n                </a>\n                <ul class=\"dropdown-menu\" ngbDropdownMenu>\n                \n                </ul>\n            </li> -->\n            <li *jhiHasAnyAuthority=\"'ROLE_ADMIN'\" ngbDropdown class=\"nav-item dropdown pointer\" routerLinkActive=\"active\" [routerLinkActiveOptions]=\"{exact: true}\">\n                <a class=\"nav-link dropdown-toggle\" ngbDropdownToggle href=\"javascript:void(0);\" id=\"admin-menu\">\n                    <span>\n                        <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i>\n                        <span>Administration</span>\n                    </span>\n                </a>\n                <ul class=\"dropdown-menu\" ngbDropdownMenu>\n                    <li>\n                        <a class=\"dropdown-item\" routerLink=\"user-management\" routerLinkActive=\"active\" (click)=\"collapseNavbar()\">\n                            <i class=\"fa fa-fw fa-user\" aria-hidden=\"true\"></i>\n                            <span>User management</span>\n                        </a>\n                    </li>\n                    <li>\n                        <a class=\"dropdown-item\" routerLink=\"jhi-metrics\" routerLinkActive=\"active\" (click)=\"collapseNavbar()\">\n                            <i class=\"fa fa-fw fa-tachometer\" aria-hidden=\"true\"></i>\n                            <span>Metrics</span>\n                        </a>\n                    </li>\n                    <li>\n                        <a class=\"dropdown-item\" routerLink=\"jhi-health\" routerLinkActive=\"active\" (click)=\"collapseNavbar()\">\n                            <i class=\"fa fa-fw fa-heart\" aria-hidden=\"true\"></i>\n                            <span>Health</span>\n                        </a>\n                    </li>\n                    <li>\n                        <a class=\"dropdown-item\" routerLink=\"jhi-configuration\" routerLinkActive=\"active\" (click)=\"collapseNavbar()\">\n                            <i class=\"fa fa-fw fa-list\" aria-hidden=\"true\"></i>\n                            <span>Configuration</span>\n                        </a>\n                    </li>\n                    <li>\n                        <a class=\"dropdown-item\" routerLink=\"audits\" routerLinkActive=\"active\" (click)=\"collapseNavbar()\">\n                            <i class=\"fa fa-fw fa-bell\" aria-hidden=\"true\"></i>\n                            <span>Audits</span>\n                        </a>\n                    </li>\n                    <li>\n                        <a class=\"dropdown-item\" routerLink=\"logs\" routerLinkActive=\"active\" (click)=\"collapseNavbar()\">\n                            <i class=\"fa fa-fw fa-tasks\" aria-hidden=\"true\"></i>\n                            <span>Logs</span>\n                        </a>\n                    </li>\n                    <li *ngIf=\"swaggerEnabled\">\n                        <a class=\"dropdown-item\" routerLink=\"docs\" routerLinkActive=\"active\" (click)=\"collapseNavbar()\">\n                            <i class=\"fa fa-fw fa-book\" aria-hidden=\"true\"></i>\n                            <span>API</span>\n                        </a>\n                    </li>\n                    <!-- jhipster-needle-add-element-to-admin-menu - JHipster will add entities to the admin menu here -->\n                </ul>\n            </li>\n            <li ngbDropdown class=\"nav-item dropdown pointer\" routerLinkActive=\"active\" [routerLinkActiveOptions]=\"{exact: true}\">\n                <a class=\"nav-link dropdown-toggle\" ngbDropdownToggle href=\"javascript:void(0);\" id=\"account-menu\">\n                  <span *ngIf=\"!getImageUrl()\">\n                    <i class=\"fa fa-user\" aria-hidden=\"true\"></i>\n                    <span>\n                      Account\n                    </span>\n                  </span>\n                  <span *ngIf=\"getImageUrl()\">\n                      <img [src]=\"getImageUrl()\" class=\"profile-image img-circle\" alt=\"Avatar\">\n                  </span>\n                </a>\n                <ul class=\"dropdown-menu dropdown-menu-right ml-auto\" ngbDropdownMenu>\n                    <li *ngSwitchCase=\"true\">\n                        <a class=\"dropdown-item\" routerLink=\"settings\" routerLinkActive=\"active\" (click)=\"collapseNavbar()\">\n                            <i class=\"fa fa-fw fa-wrench\" aria-hidden=\"true\"></i>\n                            <span>Settings</span>\n                        </a>\n                    </li>\n                    <li *ngSwitchCase=\"true\">\n                        <a class=\"dropdown-item\" routerLink=\"password\" routerLinkActive=\"active\" (click)=\"collapseNavbar()\">\n                            <i class=\"fa fa-fw fa-clock-o\" aria-hidden=\"true\"></i>\n                            <span>Password</span>\n                        </a>\n                    </li>\n                    <li *ngSwitchCase=\"true\">\n                        <a class=\"dropdown-item\" (click)=\"logout()\" id=\"logout\">\n                            <i class=\"fa fa-fw fa-sign-out\" aria-hidden=\"true\"></i>\n                            <span>Sign out</span>\n                        </a>\n                    </li>\n                    <li *ngSwitchCase=\"false\">\n                        <a class=\"dropdown-item\" (click)=\"login()\" id=\"login\">\n                            <i class=\"fa fa-fw fa-sign-in\" aria-hidden=\"true\"></i>\n                            <span>Sign in</span>\n                        </a>\n                    </li>\n                    <li *ngSwitchCase=\"false\">\n                        <a class=\"dropdown-item\" routerLink=\"register\" routerLinkActive=\"active\" (click)=\"collapseNavbar()\">\n                            <i class=\"fa fa-fw fa-user-plus\" aria-hidden=\"true\"></i>\n                            <span>Register</span>\n                        </a>\n                    </li>\n                </ul>\n            </li>\n        </ul>\n    </div>\n</nav>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/navbar/navbar.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var profile_service_1 = __webpack_require__("../../../../../src/main/webapp/app/layouts/profiles/profile.service.ts");
var shared_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var app_constants_1 = __webpack_require__("../../../../../src/main/webapp/app/app.constants.ts");
var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(loginService, principal, loginModalService, profileService, router) {
        this.loginService = loginService;
        this.principal = principal;
        this.loginModalService = loginModalService;
        this.profileService = profileService;
        this.router = router;
        this.version = app_constants_1.VERSION ? 'v' + app_constants_1.VERSION : '';
        this.isNavbarCollapsed = true;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.profileService.getProfileInfo().then(function (profileInfo) {
            _this.inProduction = profileInfo.inProduction;
            _this.swaggerEnabled = profileInfo.swaggerEnabled;
        });
    };
    NavbarComponent.prototype.collapseNavbar = function () {
        this.isNavbarCollapsed = true;
    };
    NavbarComponent.prototype.isAuthenticated = function () {
        return this.principal.isAuthenticated();
    };
    NavbarComponent.prototype.login = function () {
        this.modalRef = this.loginModalService.open();
    };
    NavbarComponent.prototype.logout = function () {
        this.collapseNavbar();
        this.loginService.logout();
        this.router.navigate(['']);
    };
    NavbarComponent.prototype.toggleNavbar = function () {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    };
    NavbarComponent.prototype.getImageUrl = function () {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    };
    NavbarComponent = __decorate([
        core_1.Component({
            selector: 'jhi-navbar',
            template: __webpack_require__("../../../../../src/main/webapp/app/layouts/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/main/webapp/app/layouts/navbar/navbar.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof shared_1.LoginService !== "undefined" && shared_1.LoginService) === "function" && _a || Object, typeof (_b = typeof shared_1.Principal !== "undefined" && shared_1.Principal) === "function" && _b || Object, typeof (_c = typeof shared_1.LoginModalService !== "undefined" && shared_1.LoginModalService) === "function" && _c || Object, typeof (_d = typeof profile_service_1.ProfileService !== "undefined" && profile_service_1.ProfileService) === "function" && _d || Object, typeof (_e = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _e || Object])
    ], NavbarComponent);
    return NavbarComponent;
    var _a, _b, _c, _d, _e;
}());
exports.NavbarComponent = NavbarComponent;
//# sourceMappingURL=navbar.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/navbar/navbar.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* ==========================================================================\nNavbar\n========================================================================== */\n.navbar-version {\n    font-size: 10px;\n    color: #ccc\n}\n\n.jh-navbar {\n    background-color: #353d47;\n    padding: .2em 1em;\n}\n\n.jh-navbar .profile-image {\n    margin: -10px 0px;\n    height: 40px;\n    width: 40px;\n    border-radius: 50%;\n}\n\n.jh-navbar .dropdown-item.active, .jh-navbar .dropdown-item.active:focus, .jh-navbar .dropdown-item.active:hover {\n    background-color: #353d47;\n}\n\n.jh-navbar .dropdown-toggle::after {\n    margin-left: 0.15em;\n}\n\n.jh-navbar ul.navbar-nav {\n    padding: 0.5em;\n}\n\n.jh-navbar .navbar-nav .nav-item {\n    margin-left: 1.5rem;\n}\n\n.jh-navbar a.nav-link {\n    font-weight: 400;\n}\n\n.jh-navbar .jh-navbar-toggler {\n    color: #ccc;\n    font-size: 1.5em;\n    padding: 10px;\n}\n\n.jh-navbar .jh-navbar-toggler:hover {\n    color: #fff;\n}\n\n@media screen and (min-width: 768px) {\n    .jh-navbar-toggler {\n        display: none;\n    }\n}\n\n@media screen and (max-width: 992px) {\n    .jh-logo-container {\n        width: 100%;\n    }\n}\n\n.navbar-title {\n    display: inline-block;\n    vertical-align: middle;\n}\n\n/* ==========================================================================\nLogo styles\n========================================================================== */\n.navbar-brand.logo {\n    padding: 5px 15px;\n}\n\n.logo .logo-img {\n    height: 45px;\n    display: inline-block;\n    vertical-align: middle;\n    width: 70px;\n}\n\n.logo-img {\n    height: 100%;\n    background: url(" + __webpack_require__("../../../../../src/main/webapp/content/images/logo-jhipster.png") + ") no-repeat center center;\n    background-size: contain;\n    width: 100%;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/navbar/navbar.route.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var navbar_component_1 = __webpack_require__("../../../../../src/main/webapp/app/layouts/navbar/navbar.component.ts");
exports.navbarRoute = {
    path: '',
    component: navbar_component_1.NavbarComponent,
    outlet: 'navbar'
};
//# sourceMappingURL=navbar.route.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/profiles/page-ribbon.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var profile_service_1 = __webpack_require__("../../../../../src/main/webapp/app/layouts/profiles/profile.service.ts");
var PageRibbonComponent = /** @class */ (function () {
    function PageRibbonComponent(profileService) {
        this.profileService = profileService;
    }
    PageRibbonComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.profileService.getProfileInfo().then(function (profileInfo) {
            _this.profileInfo = profileInfo;
            _this.ribbonEnv = profileInfo.ribbonEnv;
        });
    };
    PageRibbonComponent = __decorate([
        core_1.Component({
            selector: 'jhi-page-ribbon',
            template: "<div class=\"ribbon\" *ngIf=\"ribbonEnv\"><a href=\"\">{{ribbonEnv}}</a></div>",
            styles: [__webpack_require__("../../../../../src/main/webapp/app/layouts/profiles/page-ribbon.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof profile_service_1.ProfileService !== "undefined" && profile_service_1.ProfileService) === "function" && _a || Object])
    ], PageRibbonComponent);
    return PageRibbonComponent;
    var _a;
}());
exports.PageRibbonComponent = PageRibbonComponent;
//# sourceMappingURL=page-ribbon.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/profiles/page-ribbon.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* ==========================================================================\nDevelopement Ribbon\n========================================================================== */\n.ribbon {\n    background-color: rgba(170, 0, 0, 0.5);\n    left: -3.5em;\n    moz-transform: rotate(-45deg);\n    ms-transform: rotate(-45deg);\n    o-transform: rotate(-45deg);\n    webkit-transform: rotate(-45deg);\n    -webkit-transform: rotate(-45deg);\n            transform: rotate(-45deg);\n    overflow: hidden;\n    position: absolute;\n    top: 40px;\n    white-space: nowrap;\n    width: 15em;\n    z-index: 9999;\n    pointer-events: none;\n    opacity: 0.75;\n}\n\n.ribbon a {\n    color: #fff;\n    display: block;\n    font-weight: 400;\n    margin: 1px 0;\n    padding: 10px 50px;\n    text-align: center;\n    text-decoration: none;\n    text-shadow: 0 0 5px #444;\n    pointer-events: none;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/profiles/profile-info.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var ProfileInfo = /** @class */ (function () {
    function ProfileInfo() {
    }
    return ProfileInfo;
}());
exports.ProfileInfo = ProfileInfo;
//# sourceMappingURL=profile-info.model.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/layouts/profiles/profile.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var app_constants_1 = __webpack_require__("../../../../../src/main/webapp/app/app.constants.ts");
var profile_info_model_1 = __webpack_require__("../../../../../src/main/webapp/app/layouts/profiles/profile-info.model.ts");
var ProfileService = /** @class */ (function () {
    function ProfileService(http) {
        this.http = http;
        this.profileInfoUrl = app_constants_1.SERVER_API_URL + 'api/profile-info';
    }
    ProfileService.prototype.getProfileInfo = function () {
        if (!this.profileInfo) {
            this.profileInfo = this.http.get(this.profileInfoUrl)
                .map(function (res) {
                var data = res.json();
                var pi = new profile_info_model_1.ProfileInfo();
                pi.activeProfiles = data.activeProfiles;
                pi.ribbonEnv = data.ribbonEnv;
                pi.inProduction = data.activeProfiles.includes('prod');
                pi.swaggerEnabled = data.activeProfiles.includes('swagger');
                return pi;
            }).toPromise();
        }
        return this.profileInfo;
    };
    ProfileService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], ProfileService);
    return ProfileService;
    var _a;
}());
exports.ProfileService = ProfileService;
//# sourceMappingURL=profile.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/alert/alert-error.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var JhiAlertErrorComponent = /** @class */ (function () {
    // tslint:disable-next-line: no-unused-variable
    function JhiAlertErrorComponent(alertService, eventManager) {
        var _this = this;
        this.alertService = alertService;
        this.eventManager = eventManager;
        this.alerts = [];
        this.cleanHttpErrorListener = eventManager.subscribe('kataSgApp.httpError', function (response) {
            var i;
            var httpResponse = response.content;
            switch (httpResponse.status) {
                // connection refused, server not reachable
                case 0:
                    _this.addErrorAlert('Server not reachable', 'error.server.not.reachable');
                    break;
                case 400:
                    var arr = Array.from(httpResponse.headers._headers);
                    var headers = [];
                    for (i = 0; i < arr.length; i++) {
                        if (arr[i][0].endsWith('app-error') || arr[i][0].endsWith('app-params')) {
                            headers.push(arr[i][0]);
                        }
                    }
                    headers.sort();
                    var errorHeader = null;
                    var entityKey = null;
                    if (headers.length > 1) {
                        errorHeader = httpResponse.headers.get(headers[0]);
                        entityKey = httpResponse.headers.get(headers[1]);
                    }
                    if (errorHeader) {
                        var entityName = entityKey;
                        _this.addErrorAlert(errorHeader, errorHeader, { entityName: entityName });
                    }
                    else if (httpResponse.text() !== '' && httpResponse.json() && httpResponse.json().fieldErrors) {
                        var fieldErrors = httpResponse.json().fieldErrors;
                        for (i = 0; i < fieldErrors.length; i++) {
                            var fieldError = fieldErrors[i];
                            // convert 'something[14].other[4].id' to 'something[].other[].id' so translations can be written to it
                            var convertedField = fieldError.field.replace(/\[\d*\]/g, '[]');
                            var fieldName = convertedField.charAt(0).toUpperCase() +
                                convertedField.slice(1);
                            _this.addErrorAlert('Error on field "' + fieldName + '"', 'error.' + fieldError.message, { fieldName: fieldName });
                        }
                    }
                    else if (httpResponse.text() !== '' && httpResponse.json() && httpResponse.json().message) {
                        _this.addErrorAlert(httpResponse.json().message, httpResponse.json().message, httpResponse.json().params);
                    }
                    else {
                        _this.addErrorAlert(httpResponse.text());
                    }
                    break;
                case 404:
                    _this.addErrorAlert('Not found', 'error.url.not.found');
                    break;
                default:
                    if (httpResponse.text() !== '' && httpResponse.json() && httpResponse.json().message) {
                        _this.addErrorAlert(httpResponse.json().message);
                    }
                    else {
                        _this.addErrorAlert(httpResponse.text());
                    }
            }
        });
    }
    JhiAlertErrorComponent.prototype.ngOnDestroy = function () {
        if (this.cleanHttpErrorListener !== undefined && this.cleanHttpErrorListener !== null) {
            this.eventManager.destroy(this.cleanHttpErrorListener);
            this.alerts = [];
        }
    };
    JhiAlertErrorComponent.prototype.addErrorAlert = function (message, key, data) {
        this.alerts.push(this.alertService.addAlert({
            type: 'danger',
            msg: message,
            timeout: 5000,
            toast: this.alertService.isToast(),
            scoped: true
        }, this.alerts));
    };
    JhiAlertErrorComponent = __decorate([
        core_1.Component({
            selector: 'jhi-alert-error',
            template: "\n        <div class=\"alerts\" role=\"alert\">\n            <div *ngFor=\"let alert of alerts\"  [ngClass]=\"{'alert.position': true, 'toast': alert.toast}\">\n                <ngb-alert *ngIf=\"alert && alert.type && alert.msg\" [type]=\"alert.type\" (close)=\"alert.close(alerts)\">\n                    <pre [innerHTML]=\"alert.msg\"></pre>\n                </ngb-alert>\n            </div>\n        </div>"
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof ng_jhipster_1.JhiAlertService !== "undefined" && ng_jhipster_1.JhiAlertService) === "function" && _a || Object, typeof (_b = typeof ng_jhipster_1.JhiEventManager !== "undefined" && ng_jhipster_1.JhiEventManager) === "function" && _b || Object])
    ], JhiAlertErrorComponent);
    return JhiAlertErrorComponent;
    var _a, _b;
}());
exports.JhiAlertErrorComponent = JhiAlertErrorComponent;
//# sourceMappingURL=alert-error.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/alert/alert.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var JhiAlertComponent = /** @class */ (function () {
    function JhiAlertComponent(alertService) {
        this.alertService = alertService;
    }
    JhiAlertComponent.prototype.ngOnInit = function () {
        this.alerts = this.alertService.get();
    };
    JhiAlertComponent.prototype.ngOnDestroy = function () {
        this.alerts = [];
    };
    JhiAlertComponent = __decorate([
        core_1.Component({
            selector: 'jhi-alert',
            template: "\n        <div class=\"alerts\" role=\"alert\">\n            <div *ngFor=\"let alert of alerts\" [ngClass]=\"{'alert.position': true, 'toast': alert.toast}\">\n                <ngb-alert *ngIf=\"alert && alert.type && alert.msg\" [type]=\"alert.type\" (close)=\"alert.close(alerts)\">\n                    <pre [innerHTML]=\"alert.msg\"></pre>\n                </ngb-alert>\n            </div>\n        </div>"
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof ng_jhipster_1.JhiAlertService !== "undefined" && ng_jhipster_1.JhiAlertService) === "function" && _a || Object])
    ], JhiAlertComponent);
    return JhiAlertComponent;
    var _a;
}());
exports.JhiAlertComponent = JhiAlertComponent;
//# sourceMappingURL=alert.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/auth/account.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var app_constants_1 = __webpack_require__("../../../../../src/main/webapp/app/app.constants.ts");
var AccountService = /** @class */ (function () {
    function AccountService(http) {
        this.http = http;
    }
    AccountService.prototype.get = function () {
        return this.http.get(app_constants_1.SERVER_API_URL + 'api/account').map(function (res) { return res.json(); });
    };
    AccountService.prototype.save = function (account) {
        return this.http.post(app_constants_1.SERVER_API_URL + 'api/account', account);
    };
    AccountService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], AccountService);
    return AccountService;
    var _a;
}());
exports.AccountService = AccountService;
//# sourceMappingURL=account.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/auth/auth-jwt.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var Rx_1 = __webpack_require__("../../../../rxjs/Rx.js");
var ng2_webstorage_1 = __webpack_require__("../../../../ng2-webstorage/dist/app.js");
var app_constants_1 = __webpack_require__("../../../../../src/main/webapp/app/app.constants.ts");
var AuthServerProvider = /** @class */ (function () {
    function AuthServerProvider(http, $localStorage, $sessionStorage) {
        this.http = http;
        this.$localStorage = $localStorage;
        this.$sessionStorage = $sessionStorage;
    }
    AuthServerProvider.prototype.getToken = function () {
        return this.$localStorage.retrieve('authenticationToken') || this.$sessionStorage.retrieve('authenticationToken');
    };
    AuthServerProvider.prototype.login = function (credentials) {
        var data = {
            username: credentials.username,
            password: credentials.password,
            rememberMe: credentials.rememberMe
        };
        return this.http.post(app_constants_1.SERVER_API_URL + 'api/authenticate', data).map(authenticateSuccess.bind(this));
        function authenticateSuccess(resp) {
            var bearerToken = resp.headers.get('Authorization');
            if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
                var jwt = bearerToken.slice(7, bearerToken.length);
                this.storeAuthenticationToken(jwt, credentials.rememberMe);
                return jwt;
            }
        }
    };
    AuthServerProvider.prototype.loginWithToken = function (jwt, rememberMe) {
        if (jwt) {
            this.storeAuthenticationToken(jwt, rememberMe);
            return Promise.resolve(jwt);
        }
        else {
            return Promise.reject('auth-jwt-service Promise reject'); // Put appropriate error message here
        }
    };
    AuthServerProvider.prototype.storeAuthenticationToken = function (jwt, rememberMe) {
        if (rememberMe) {
            this.$localStorage.store('authenticationToken', jwt);
        }
        else {
            this.$sessionStorage.store('authenticationToken', jwt);
        }
    };
    AuthServerProvider.prototype.logout = function () {
        var _this = this;
        return new Rx_1.Observable(function (observer) {
            _this.$localStorage.clear('authenticationToken');
            _this.$sessionStorage.clear('authenticationToken');
            observer.complete();
        });
    };
    AuthServerProvider = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object, typeof (_b = typeof ng2_webstorage_1.LocalStorageService !== "undefined" && ng2_webstorage_1.LocalStorageService) === "function" && _b || Object, typeof (_c = typeof ng2_webstorage_1.SessionStorageService !== "undefined" && ng2_webstorage_1.SessionStorageService) === "function" && _c || Object])
    ], AuthServerProvider);
    return AuthServerProvider;
    var _a, _b, _c;
}());
exports.AuthServerProvider = AuthServerProvider;
//# sourceMappingURL=auth-jwt.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/auth/csrf.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ngx_cookie_1 = __webpack_require__("../../../../ngx-cookie/index.js");
var CSRFService = /** @class */ (function () {
    function CSRFService(cookieService) {
        this.cookieService = cookieService;
    }
    CSRFService.prototype.getCSRF = function (name) {
        name = "" + (name ? name : 'XSRF-TOKEN');
        return this.cookieService.get(name);
    };
    CSRFService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof ngx_cookie_1.CookieService !== "undefined" && ngx_cookie_1.CookieService) === "function" && _a || Object])
    ], CSRFService);
    return CSRFService;
    var _a;
}());
exports.CSRFService = CSRFService;
//# sourceMappingURL=csrf.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/auth/has-any-authority.directive.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var principal_service_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/auth/principal.service.ts");
/**
 * @whatItDoes Conditionally includes an HTML element if current user has any
 * of the authorities passed as the `expression`.
 *
 * @howToUse
 * ```
 *     <some-element *jhiHasAnyAuthority="'ROLE_ADMIN'">...</some-element>
 *
 *     <some-element *jhiHasAnyAuthority="['ROLE_ADMIN', 'ROLE_USER']">...</some-element>
 * ```
 */
var HasAnyAuthorityDirective = /** @class */ (function () {
    function HasAnyAuthorityDirective(principal, templateRef, viewContainerRef) {
        this.principal = principal;
        this.templateRef = templateRef;
        this.viewContainerRef = viewContainerRef;
    }
    Object.defineProperty(HasAnyAuthorityDirective.prototype, "jhiHasAnyAuthority", {
        set: function (value) {
            var _this = this;
            this.authorities = typeof value === 'string' ? [value] : value;
            this.updateView();
            // Get notified each time authentication state changes.
            this.principal.getAuthenticationState().subscribe(function (identity) { return _this.updateView(); });
        },
        enumerable: true,
        configurable: true
    });
    HasAnyAuthorityDirective.prototype.updateView = function () {
        var _this = this;
        this.principal.hasAnyAuthority(this.authorities).then(function (result) {
            _this.viewContainerRef.clear();
            if (result) {
                _this.viewContainerRef.createEmbeddedView(_this.templateRef);
            }
        });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], HasAnyAuthorityDirective.prototype, "jhiHasAnyAuthority", null);
    HasAnyAuthorityDirective = __decorate([
        core_1.Directive({
            selector: '[jhiHasAnyAuthority]'
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof principal_service_1.Principal !== "undefined" && principal_service_1.Principal) === "function" && _a || Object, typeof (_b = typeof core_1.TemplateRef !== "undefined" && core_1.TemplateRef) === "function" && _b || Object, typeof (_c = typeof core_1.ViewContainerRef !== "undefined" && core_1.ViewContainerRef) === "function" && _c || Object])
    ], HasAnyAuthorityDirective);
    return HasAnyAuthorityDirective;
    var _a, _b, _c;
}());
exports.HasAnyAuthorityDirective = HasAnyAuthorityDirective;
//# sourceMappingURL=has-any-authority.directive.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/auth/principal.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var Subject_1 = __webpack_require__("../../../../rxjs/Subject.js");
var account_service_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/auth/account.service.ts");
var Principal = /** @class */ (function () {
    function Principal(account) {
        this.account = account;
        this.authenticated = false;
        this.authenticationState = new Subject_1.Subject();
    }
    Principal.prototype.authenticate = function (identity) {
        this.userIdentity = identity;
        this.authenticated = identity !== null;
        this.authenticationState.next(this.userIdentity);
    };
    Principal.prototype.hasAnyAuthority = function (authorities) {
        return Promise.resolve(this.hasAnyAuthorityDirect(authorities));
    };
    Principal.prototype.hasAnyAuthorityDirect = function (authorities) {
        if (!this.authenticated || !this.userIdentity || !this.userIdentity.authorities) {
            return false;
        }
        for (var i = 0; i < authorities.length; i++) {
            if (this.userIdentity.authorities.includes(authorities[i])) {
                return true;
            }
        }
        return false;
    };
    Principal.prototype.hasAuthority = function (authority) {
        if (!this.authenticated) {
            return Promise.resolve(false);
        }
        return this.identity().then(function (id) {
            return Promise.resolve(id.authorities && id.authorities.includes(authority));
        }, function () {
            return Promise.resolve(false);
        });
    };
    Principal.prototype.identity = function (force) {
        var _this = this;
        if (force === true) {
            this.userIdentity = undefined;
        }
        // check and see if we have retrieved the userIdentity data from the server.
        // if we have, reuse it by immediately resolving
        if (this.userIdentity) {
            return Promise.resolve(this.userIdentity);
        }
        // retrieve the userIdentity data from the server, update the identity object, and then resolve.
        return this.account.get().toPromise().then(function (account) {
            if (account) {
                _this.userIdentity = account;
                _this.authenticated = true;
            }
            else {
                _this.userIdentity = null;
                _this.authenticated = false;
            }
            _this.authenticationState.next(_this.userIdentity);
            return _this.userIdentity;
        }).catch(function (err) {
            _this.userIdentity = null;
            _this.authenticated = false;
            _this.authenticationState.next(_this.userIdentity);
            return null;
        });
    };
    Principal.prototype.isAuthenticated = function () {
        return this.authenticated;
    };
    Principal.prototype.isIdentityResolved = function () {
        return this.userIdentity !== undefined;
    };
    Principal.prototype.getAuthenticationState = function () {
        return this.authenticationState.asObservable();
    };
    Principal.prototype.getImageUrl = function () {
        return this.isIdentityResolved() ? this.userIdentity.imageUrl : null;
    };
    Principal = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof account_service_1.AccountService !== "undefined" && account_service_1.AccountService) === "function" && _a || Object])
    ], Principal);
    return Principal;
    var _a;
}());
exports.Principal = Principal;
//# sourceMappingURL=principal.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/auth/state-storage.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ng2_webstorage_1 = __webpack_require__("../../../../ng2-webstorage/dist/app.js");
var StateStorageService = /** @class */ (function () {
    function StateStorageService($sessionStorage) {
        this.$sessionStorage = $sessionStorage;
    }
    StateStorageService.prototype.getPreviousState = function () {
        return this.$sessionStorage.retrieve('previousState');
    };
    StateStorageService.prototype.resetPreviousState = function () {
        this.$sessionStorage.clear('previousState');
    };
    StateStorageService.prototype.storePreviousState = function (previousStateName, previousStateParams) {
        var previousState = { 'name': previousStateName, 'params': previousStateParams };
        this.$sessionStorage.store('previousState', previousState);
    };
    StateStorageService.prototype.getDestinationState = function () {
        return this.$sessionStorage.retrieve('destinationState');
    };
    StateStorageService.prototype.storeUrl = function (url) {
        this.$sessionStorage.store('previousUrl', url);
    };
    StateStorageService.prototype.getUrl = function () {
        return this.$sessionStorage.retrieve('previousUrl');
    };
    StateStorageService.prototype.storeDestinationState = function (destinationState, destinationStateParams, fromState) {
        var destinationInfo = {
            'destination': {
                'name': destinationState.name,
                'data': destinationState.data,
            },
            'params': destinationStateParams,
            'from': {
                'name': fromState.name,
            }
        };
        this.$sessionStorage.store('destinationState', destinationInfo);
    };
    StateStorageService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof ng2_webstorage_1.SessionStorageService !== "undefined" && ng2_webstorage_1.SessionStorageService) === "function" && _a || Object])
    ], StateStorageService);
    return StateStorageService;
    var _a;
}());
exports.StateStorageService = StateStorageService;
//# sourceMappingURL=state-storage.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/auth/user-route-access-service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var _1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var login_modal_service_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/login/login-modal.service.ts");
var state_storage_service_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/auth/state-storage.service.ts");
var UserRouteAccessService = /** @class */ (function () {
    function UserRouteAccessService(router, loginModalService, principal, stateStorageService) {
        this.router = router;
        this.loginModalService = loginModalService;
        this.principal = principal;
        this.stateStorageService = stateStorageService;
    }
    UserRouteAccessService.prototype.canActivate = function (route, state) {
        var authorities = route.data['authorities'];
        // We need to call the checkLogin / and so the principal.identity() function, to ensure,
        // that the client has a principal too, if they already logged in by the server.
        // This could happen on a page refresh.
        return this.checkLogin(authorities, state.url);
    };
    UserRouteAccessService.prototype.checkLogin = function (authorities, url) {
        var _this = this;
        var principal = this.principal;
        return Promise.resolve(principal.identity().then(function (account) {
            if (!authorities || authorities.length === 0) {
                return true;
            }
            if (account) {
                return principal.hasAnyAuthority(authorities).then(function (response) {
                    if (response) {
                        return true;
                    }
                    return false;
                });
            }
            _this.stateStorageService.storeUrl(url);
            _this.router.navigate(['accessdenied']).then(function () {
                // only show the login dialog, if the user hasn't logged in yet
                if (!account) {
                    _this.loginModalService.open();
                }
            });
            return false;
        }));
    };
    UserRouteAccessService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _a || Object, typeof (_b = typeof login_modal_service_1.LoginModalService !== "undefined" && login_modal_service_1.LoginModalService) === "function" && _b || Object, typeof (_c = typeof _1.Principal !== "undefined" && _1.Principal) === "function" && _c || Object, typeof (_d = typeof state_storage_service_1.StateStorageService !== "undefined" && state_storage_service_1.StateStorageService) === "function" && _d || Object])
    ], UserRouteAccessService);
    return UserRouteAccessService;
    var _a, _b, _c, _d;
}());
exports.UserRouteAccessService = UserRouteAccessService;
//# sourceMappingURL=user-route-access-service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/constants/error.constants.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.PROBLEM_BASE_URL = 'http://www.jhipster.tech/problem';
exports.EMAIL_ALREADY_USED_TYPE = exports.PROBLEM_BASE_URL + '/email-already-used';
exports.LOGIN_ALREADY_USED_TYPE = exports.PROBLEM_BASE_URL + '/login-already-used';
exports.EMAIL_NOT_FOUND_TYPE = exports.PROBLEM_BASE_URL + '/email-not-found';
//# sourceMappingURL=error.constants.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/constants/pagination.constants.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.ITEMS_PER_PAGE = 20;
//# sourceMappingURL=pagination.constants.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/index.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/constants/error.constants.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/constants/pagination.constants.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/alert/alert.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/alert/alert-error.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/auth/csrf.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/auth/state-storage.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/auth/account.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/auth/auth-jwt.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/auth/principal.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/auth/has-any-authority.directive.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/auth/user-route-access-service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/login/login.component.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/login/login-modal.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/login/login.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/user/account.model.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/user/compte.model.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/user/compte.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/user/user.model.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/user/user.service.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/model/response-wrapper.model.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/model/request-util.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/model/base-entity.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/shared-libs.module.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/shared-common.module.ts"));
__export(__webpack_require__("../../../../../src/main/webapp/app/shared/shared.module.ts"));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/login/login-modal.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var login_component_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/login/login.component.ts");
var LoginModalService = /** @class */ (function () {
    function LoginModalService(modalService) {
        this.modalService = modalService;
        this.isOpen = false;
    }
    LoginModalService.prototype.open = function () {
        var _this = this;
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        var modalRef = this.modalService.open(login_component_1.JhiLoginModalComponent, {
            container: 'nav'
        });
        modalRef.result.then(function (result) {
            _this.isOpen = false;
        }, function (reason) {
            _this.isOpen = false;
        });
        return modalRef;
    };
    LoginModalService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof ng_bootstrap_1.NgbModal !== "undefined" && ng_bootstrap_1.NgbModal) === "function" && _a || Object])
    ], LoginModalService);
    return LoginModalService;
    var _a;
}());
exports.LoginModalService = LoginModalService;
//# sourceMappingURL=login-modal.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\n    <h4 class=\"modal-title\">Sign in</h4>\n    <button aria-label=\"Close\" data-dismiss=\"modal\" class=\"close\" type=\"button\" (click)=\"activeModal.dismiss('closed')\"><span aria-hidden=\"true\">x</span>\n    </button>\n</div>\n<div class=\"modal-body\">\n    <div class=\"row justify-content-center\">\n        <div class=\"col-md-8\">\n            <div class=\"alert alert-danger\" *ngIf=\"authenticationError\">\n                <strong>Failed to sign in!</strong> Please check your credentials and try again.\n            </div>\n        </div>\n        <div class=\"col-md-8\">\n            <form class=\"form\" role=\"form\" (ngSubmit)=\"login()\">\n                <div class=\"form-group\">\n                    <label for=\"username\">Login</label>\n                    <input type=\"text\" class=\"form-control\" name=\"username\" id=\"username\" placeholder=\"Your username\"\n                    [(ngModel)]=\"username\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"password\">Password</label>\n                    <input type=\"password\" class=\"form-control\" name=\"password\" id=\"password\" placeholder=\"Your password\"\n                           [(ngModel)]=\"password\">\n                </div>\n                <div class=\"form-check\">\n                    <label class=\"form-check-label\" for=\"rememberMe\">\n                        <input class=\"form-check-input\" type=\"checkbox\" name=\"rememberMe\" id=\"rememberMe\" [(ngModel)]=\"rememberMe\" checked>\n                        <span>Remember me</span>\n                    </label>\n                </div>\n                <button type=\"submit\" class=\"btn btn-primary\">Sign in</button>\n            </form>\n            <p></p>\n            <div class=\"alert alert-warning\">\n                <a class=\"alert-link\" (click)=\"requestResetPassword()\">Did you forget your password?</a>\n            </div>\n            <div class=\"alert alert-warning\">\n                <span>You don't have an account yet?</span>\n                <a class=\"alert-link\" (click)=\"register()\">Register a new account</a>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/login/login.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var router_1 = __webpack_require__("../../../router/@angular/router.es5.js");
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var login_service_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/login/login.service.ts");
var state_storage_service_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/auth/state-storage.service.ts");
var JhiLoginModalComponent = /** @class */ (function () {
    function JhiLoginModalComponent(eventManager, loginService, stateStorageService, elementRef, renderer, router, activeModal) {
        this.eventManager = eventManager;
        this.loginService = loginService;
        this.stateStorageService = stateStorageService;
        this.elementRef = elementRef;
        this.renderer = renderer;
        this.router = router;
        this.activeModal = activeModal;
        this.credentials = {};
    }
    JhiLoginModalComponent.prototype.ngAfterViewInit = function () {
        this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#username'), 'focus', []);
    };
    JhiLoginModalComponent.prototype.cancel = function () {
        this.credentials = {
            username: null,
            password: null,
            rememberMe: true
        };
        this.authenticationError = false;
        this.activeModal.dismiss('cancel');
    };
    JhiLoginModalComponent.prototype.login = function () {
        var _this = this;
        this.loginService.login({
            username: this.username,
            password: this.password,
            rememberMe: this.rememberMe
        }).then(function () {
            _this.authenticationError = false;
            _this.activeModal.dismiss('login success');
            if (_this.router.url === '/register' || (/^\/activate\//.test(_this.router.url)) ||
                (/^\/reset\//.test(_this.router.url))) {
                _this.router.navigate(['']);
            }
            _this.eventManager.broadcast({
                name: 'authenticationSuccess',
                content: 'Sending Authentication Success'
            });
            // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
            // // since login is succesful, go to stored previousState and clear previousState
            var redirect = _this.stateStorageService.getUrl();
            if (redirect) {
                _this.stateStorageService.storeUrl(null);
                _this.router.navigate([redirect]);
            }
        }).catch(function () {
            _this.authenticationError = true;
        });
    };
    JhiLoginModalComponent.prototype.register = function () {
        this.activeModal.dismiss('to state register');
        this.router.navigate(['/register']);
    };
    JhiLoginModalComponent.prototype.requestResetPassword = function () {
        this.activeModal.dismiss('to state requestReset');
        this.router.navigate(['/reset', 'request']);
    };
    JhiLoginModalComponent = __decorate([
        core_1.Component({
            selector: 'jhi-login-modal',
            template: __webpack_require__("../../../../../src/main/webapp/app/shared/login/login.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof ng_jhipster_1.JhiEventManager !== "undefined" && ng_jhipster_1.JhiEventManager) === "function" && _a || Object, typeof (_b = typeof login_service_1.LoginService !== "undefined" && login_service_1.LoginService) === "function" && _b || Object, typeof (_c = typeof state_storage_service_1.StateStorageService !== "undefined" && state_storage_service_1.StateStorageService) === "function" && _c || Object, typeof (_d = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _d || Object, typeof (_e = typeof core_1.Renderer !== "undefined" && core_1.Renderer) === "function" && _e || Object, typeof (_f = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _f || Object, typeof (_g = typeof ng_bootstrap_1.NgbActiveModal !== "undefined" && ng_bootstrap_1.NgbActiveModal) === "function" && _g || Object])
    ], JhiLoginModalComponent);
    return JhiLoginModalComponent;
    var _a, _b, _c, _d, _e, _f, _g;
}());
exports.JhiLoginModalComponent = JhiLoginModalComponent;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/login/login.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var principal_service_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/auth/principal.service.ts");
var auth_jwt_service_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/auth/auth-jwt.service.ts");
var LoginService = /** @class */ (function () {
    function LoginService(principal, authServerProvider) {
        this.principal = principal;
        this.authServerProvider = authServerProvider;
    }
    LoginService.prototype.login = function (credentials, callback) {
        var _this = this;
        var cb = callback || function () { };
        return new Promise(function (resolve, reject) {
            _this.authServerProvider.login(credentials).subscribe(function (data) {
                _this.principal.identity(true).then(function (account) {
                    resolve(data);
                });
                return cb();
            }, function (err) {
                _this.logout();
                reject(err);
                return cb(err);
            });
        });
    };
    LoginService.prototype.loginWithToken = function (jwt, rememberMe) {
        return this.authServerProvider.loginWithToken(jwt, rememberMe);
    };
    LoginService.prototype.logout = function () {
        this.authServerProvider.logout().subscribe();
        this.principal.authenticate(null);
    };
    LoginService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof principal_service_1.Principal !== "undefined" && principal_service_1.Principal) === "function" && _a || Object, typeof (_b = typeof auth_jwt_service_1.AuthServerProvider !== "undefined" && auth_jwt_service_1.AuthServerProvider) === "function" && _b || Object])
    ], LoginService);
    return LoginService;
    var _a, _b;
}());
exports.LoginService = LoginService;
//# sourceMappingURL=login.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/model/base-entity.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
;
//# sourceMappingURL=base-entity.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/model/request-util.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
exports.createRequestOption = function (req) {
    var options = new http_1.BaseRequestOptions();
    if (req) {
        var params = new http_1.URLSearchParams();
        params.set('page', req.page);
        params.set('size', req.size);
        if (req.sort) {
            params.paramsMap.set('sort', req.sort);
        }
        params.set('query', req.query);
        options.params = params;
    }
    return options;
};
//# sourceMappingURL=request-util.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/model/response-wrapper.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var ResponseWrapper = /** @class */ (function () {
    function ResponseWrapper(headers, json, status) {
        this.headers = headers;
        this.json = json;
        this.status = status;
    }
    return ResponseWrapper;
}());
exports.ResponseWrapper = ResponseWrapper;
//# sourceMappingURL=response-wrapper.model.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/shared-common.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var platform_browser_1 = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
var _1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var KataSgSharedCommonModule = /** @class */ (function () {
    function KataSgSharedCommonModule() {
    }
    KataSgSharedCommonModule = __decorate([
        core_1.NgModule({
            imports: [
                _1.KataSgSharedLibsModule
            ],
            declarations: [
                _1.JhiAlertComponent,
                _1.JhiAlertErrorComponent
            ],
            providers: [
                platform_browser_1.Title,
                {
                    provide: core_1.LOCALE_ID,
                    useValue: 'en'
                },
            ],
            exports: [
                _1.KataSgSharedLibsModule,
                _1.JhiAlertComponent,
                _1.JhiAlertErrorComponent
            ]
        })
    ], KataSgSharedCommonModule);
    return KataSgSharedCommonModule;
}());
exports.KataSgSharedCommonModule = KataSgSharedCommonModule;
//# sourceMappingURL=shared-common.module.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/shared-libs.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var forms_1 = __webpack_require__("../../../forms/@angular/forms.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var common_1 = __webpack_require__("../../../common/@angular/common.es5.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var ng_jhipster_1 = __webpack_require__("../../../../ng-jhipster/index.js");
var ngx_infinite_scroll_1 = __webpack_require__("../../../../ngx-infinite-scroll/modules/ngx-infinite-scroll.es5.js");
var ngx_cookie_1 = __webpack_require__("../../../../ngx-cookie/index.js");
var KataSgSharedLibsModule = /** @class */ (function () {
    function KataSgSharedLibsModule() {
    }
    KataSgSharedLibsModule = __decorate([
        core_1.NgModule({
            imports: [
                ng_bootstrap_1.NgbModule.forRoot(),
                ng_jhipster_1.NgJhipsterModule.forRoot({
                    // set below to true to make alerts look like toast
                    alertAsToast: false,
                }),
                ngx_infinite_scroll_1.InfiniteScrollModule,
                ngx_cookie_1.CookieModule.forRoot()
            ],
            exports: [
                forms_1.FormsModule,
                http_1.HttpModule,
                common_1.CommonModule,
                ng_bootstrap_1.NgbModule,
                ng_jhipster_1.NgJhipsterModule,
                ngx_infinite_scroll_1.InfiniteScrollModule
            ]
        })
    ], KataSgSharedLibsModule);
    return KataSgSharedLibsModule;
}());
exports.KataSgSharedLibsModule = KataSgSharedLibsModule;
//# sourceMappingURL=shared-libs.module.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/shared.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var common_1 = __webpack_require__("../../../common/@angular/common.es5.js");
var _1 = __webpack_require__("../../../../../src/main/webapp/app/shared/index.ts");
var KataSgSharedModule = /** @class */ (function () {
    function KataSgSharedModule() {
    }
    KataSgSharedModule = __decorate([
        core_1.NgModule({
            imports: [
                _1.KataSgSharedLibsModule,
                _1.KataSgSharedCommonModule
            ],
            declarations: [
                _1.JhiLoginModalComponent,
                _1.HasAnyAuthorityDirective
            ],
            providers: [
                _1.LoginService,
                _1.LoginModalService,
                _1.AccountService,
                _1.StateStorageService,
                _1.Principal,
                _1.CSRFService,
                _1.AuthServerProvider,
                _1.UserService,
                common_1.DatePipe,
                _1.CompteService
            ],
            entryComponents: [_1.JhiLoginModalComponent],
            exports: [
                _1.KataSgSharedCommonModule,
                _1.JhiLoginModalComponent,
                _1.HasAnyAuthorityDirective,
                common_1.DatePipe
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        })
    ], KataSgSharedModule);
    return KataSgSharedModule;
}());
exports.KataSgSharedModule = KataSgSharedModule;
//# sourceMappingURL=shared.module.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/user/account.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Account = /** @class */ (function () {
    function Account(activated, authorities, email, firstName, langKey, lastName, login, imageUrl) {
        this.activated = activated;
        this.authorities = authorities;
        this.email = email;
        this.firstName = firstName;
        this.langKey = langKey;
        this.lastName = lastName;
        this.login = login;
        this.imageUrl = imageUrl;
    }
    return Account;
}());
exports.Account = Account;
//# sourceMappingURL=account.model.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/user/compte.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Compte = /** @class */ (function () {
    function Compte(id, amount) {
        this.id = id ? id : 0;
        this.amount = amount ? amount : 0;
    }
    return Compte;
}());
exports.Compte = Compte;
//# sourceMappingURL=compte.model.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/user/compte.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var app_constants_1 = __webpack_require__("../../../../../src/main/webapp/app/app.constants.ts");
var response_wrapper_model_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/model/response-wrapper.model.ts");
var CompteService = /** @class */ (function () {
    function CompteService(http) {
        this.http = http;
    }
    CompteService.prototype.getCompte = function () {
        return this.http.get(app_constants_1.SERVER_API_URL + 'api/userCompte').map(function (res) { return res.json(); });
    };
    CompteService.prototype.save = function (updateamount) {
        return this.http.post(app_constants_1.SERVER_API_URL + 'api/updateCompte', updateamount);
    };
    CompteService.prototype.update = function (amount) {
        var _this = this;
        return this.http.put(app_constants_1.SERVER_API_URL + 'api/updateCompte', amount)
            .map(function (res) { return _this.convertResponse(res); });
    };
    CompteService.prototype.convertResponse = function (res) {
        var jsonResponse = res.json();
        return new response_wrapper_model_1.ResponseWrapper(res.headers, jsonResponse, res.status);
    };
    CompteService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], CompteService);
    return CompteService;
    var _a;
}());
exports.CompteService = CompteService;
//# sourceMappingURL=compte.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/user/user.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var User = /** @class */ (function () {
    function User(id, login, firstName, lastName, email, activated, langKey, authorities, createdBy, createdDate, lastModifiedBy, lastModifiedDate, password) {
        this.id = id ? id : null;
        this.login = login ? login : null;
        this.firstName = firstName ? firstName : null;
        this.lastName = lastName ? lastName : null;
        this.email = email ? email : null;
        this.activated = activated ? activated : false;
        this.langKey = langKey ? langKey : null;
        this.authorities = authorities ? authorities : null;
        this.createdBy = createdBy ? createdBy : null;
        this.createdDate = createdDate ? createdDate : null;
        this.lastModifiedBy = lastModifiedBy ? lastModifiedBy : null;
        this.lastModifiedDate = lastModifiedDate ? lastModifiedDate : null;
        this.password = password ? password : null;
    }
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.model.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/shared/user/user.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var app_constants_1 = __webpack_require__("../../../../../src/main/webapp/app/app.constants.ts");
var response_wrapper_model_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/model/response-wrapper.model.ts");
var request_util_1 = __webpack_require__("../../../../../src/main/webapp/app/shared/model/request-util.ts");
var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.resourceUrl = app_constants_1.SERVER_API_URL + 'api/users';
    }
    UserService.prototype.create = function (user) {
        var _this = this;
        return this.http.post(this.resourceUrl, user)
            .map(function (res) { return _this.convertResponse(res); });
    };
    UserService.prototype.update = function (user) {
        var _this = this;
        return this.http.put(this.resourceUrl, user)
            .map(function (res) { return _this.convertResponse(res); });
    };
    UserService.prototype.find = function (login) {
        return this.http.get(this.resourceUrl + "/" + login).map(function (res) { return res.json(); });
    };
    UserService.prototype.query = function (req) {
        var _this = this;
        var options = request_util_1.createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map(function (res) { return _this.convertResponse(res); });
    };
    UserService.prototype.delete = function (login) {
        return this.http.delete(this.resourceUrl + "/" + login);
    };
    UserService.prototype.authorities = function () {
        return this.http.get(app_constants_1.SERVER_API_URL + 'api/users/authorities').map(function (res) {
            var json = res.json();
            return json;
        });
    };
    UserService.prototype.convertResponse = function (res) {
        var jsonResponse = res.json();
        return new response_wrapper_model_1.ResponseWrapper(res.headers, jsonResponse, res.status);
    };
    UserService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], UserService);
    return UserService;
    var _a;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/app/vendor.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
/* after changing this file run 'yarn run webpack:build' */
/* tslint:disable */
__webpack_require__("../../../../../src/main/webapp/content/css/vendor.css");
// jhipster-needle-add-element-to-vendor - JHipster will add new menu items here
//# sourceMappingURL=vendor.js.map

/***/ }),

/***/ "../../../../../src/main/webapp/content/css/vendor.css":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("../../../../css-loader/index.js?{\"sourceMap\":false,\"importLoaders\":1}!../../../../postcss-loader/index.js?{\"ident\":\"postcss\"}!../../../../../src/main/webapp/content/css/vendor.css");
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__("../../../../style-loader/lib/addStyles.js")(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/css-loader/index.js??ref--8-1!../../../../../node_modules/postcss-loader/index.js??postcss!./vendor.css", function() {
			var newContent = require("!!../../../../../node_modules/css-loader/index.js??ref--8-1!../../../../../node_modules/postcss-loader/index.js??postcss!./vendor.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "../../../../../src/main/webapp/content/images/hipster.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "hipster.e05ebd4f7fad0a8a00af.png";

/***/ }),

/***/ "../../../../../src/main/webapp/content/images/hipster2x.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "hipster2x.1cd3a1d782e85ba37677.png";

/***/ }),

/***/ "../../../../../src/main/webapp/content/images/logo-jhipster.png":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF4AAAB3CAYAAACKcfeQAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAAShJREFUeF7tyKERADAQw7Dff+l0AVNfiYGIbls+wIwPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPMz7M+DDjw4wPM7bdA5XXmomaJUhrAAAAAElFTkSuQmCC"

/***/ }),

/***/ "../../../../css-loader/index.js?{\"sourceMap\":false,\"importLoaders\":1}!../../../../postcss-loader/index.js?{\"ident\":\"postcss\"}!../../../../../src/main/webapp/content/css/vendor.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__("../../../../css-loader/index.js?{\"sourceMap\":false,\"importLoaders\":1}!../../../../postcss-loader/index.js?{\"ident\":\"postcss\"}!../../../../bootstrap/dist/css/bootstrap.min.css"), "");
exports.i(__webpack_require__("../../../../css-loader/index.js?{\"sourceMap\":false,\"importLoaders\":1}!../../../../postcss-loader/index.js?{\"ident\":\"postcss\"}!../../../../font-awesome/css/font-awesome.css"), "");

// module
exports.push([module.i, "/* after changing this file run 'yarn run webpack:build' */\n\n", ""]);

// exports


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main/webapp/app/app.main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map